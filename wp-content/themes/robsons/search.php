<?php

get_header() ?>
  <body>

  <div class="posts_area">
	<section class="breadcrumb-section breadcrumb-section_insurance" style="background-image: url(url(http://woocommerce-158890-504939.cloudwaysapps.com/wp-content/uploads/2018/05/breadcrumb-diamond-broker-bg.png));">
	  <div class="container">
		<div class="wrapper">
		  <div class="breadcrumb-section__title">Search</div>
		  <ul class="breadcrumb">
			<li class="breadcrumb__item"><a href="/">Home</a></li>
			<li class="breadcrumb__item"><span>Search</span></li>
		  </ul>
		</div>
	  </div>
	</section>
	<div class="container">
	  <section class="advantages">
		<div style="padding-top: 40px;" class="categories-post__title">
		  <?php echo 'Search results: ' . '<span>' . get_search_query() . '</span>'; ?>
		</div>

		<?php
		if (have_posts()) :
		  while (have_posts()) : the_post(); ?>
			<div style='font-size: 16px;color: #808080;font-weight: 400;font-family: "Montserrat", sans-serif; line-height: 22px; padding-top: 25px; margin: 10px 0 30px;'>
			  <h2><a style="font-size: 20px;" class="categories-post__title" href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
			  <div style="margin-top: 10px;" id="post_info">
				<div>Author: <?php the_author() ?></div>
				<div>Date Added: <?php the_date() ?></div>
				<div id="clear"></div>
			  </div>
			  <p><?php the_excerpt() ?></p>
			</div>
			<hr>
		  <?php endwhile; ?>
		<?php
		else :
		  echo "Sorry for your result: nothing found";
		endif;
		?>

	  </section>
	</div>
  </div>

  </body>
<?php get_footer() ?>