<?php
/**
 * A Simple Category Template
 */

get_header(); ?>
<body>
<div class="posts_area">
  <section class="breadcrumb-section breadcrumb-section_insurance" style="background-image: url(url(http://woocommerce-158890-504939.cloudwaysapps.com/wp-content/uploads/2018/05/breadcrumb-diamond-broker-bg.png));">
	<div class="container">
	  <div class="wrapper">
		<div class="breadcrumb-section__title">Category</div>
		<ul class="breadcrumb">
		  <li class="breadcrumb__item"><a href="/">Home</a></li>
		  <li class="breadcrumb__item"><span>Category</span></li>
		</ul>
	  </div>
	</div>
  </section>

  <div class="container">
	<section class="advantages">
	  <?php
	  if ( have_posts() ) : ?>
		<h1 style="padding-top: 40px;" class="categories-post__title">Category: <?php single_cat_title( '', false ); ?></h1>
		<?php
		if ( category_description() ) : ?>
		  <div class="form-subscribe__sub-title"><?php echo category_description(); ?></div>
		<?php endif; ?>

		<?php
		while ( have_posts() ) : the_post(); ?>
		<div style='font-size: 16px;color: #808080;font-weight: 400;font-family: "Montserrat", sans-serif; line-height: 22px; padding-top: 25px; margin: 10px 0 30px;'>
		  <h2><a style="font-size: 20px;" class="categories-post__title" href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
		  <small><?php the_time('F jS, Y') ?> by <?php the_author_posts_link() ?></small>
		  <?php the_content(); ?>
		  <hr>
		<?php endwhile;

	  else: ?>
		<p>Sorry, no posts matched your criteria.</p>
		</div>

	  <?php endif; ?>
	</section>
  </div>
</div>
</body>
<?php get_footer(); ?>