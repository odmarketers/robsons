jQuery( document ).ready(function($) {

    // Check Sign Up
    $('body').on('click', '.form-sign-up__wrapper #sgn_submit' ,function(e){
        e.preventDefault();

        var error = false;
        var name  = $('#sgn_name').val();
        var email = $('#sgn_email').val();

        if( $('#sgn_name').val() == '' ) {
            $('#sgn_name').parent().css('border', '1px solid red');
            var error = true;
        } else {
            $('#sgn_name').parent().css('border', '0px solid #d0d2d3');
        }

        var email = $('#sgn_email').val();
        regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if(!regex.test(email)){
            $('#sgn_email').parent().css('border', '1px solid red');
            var error = true;
        } else {
            $('#sgn_email').parent().css('border', '0px solid #d0d2d3');
        }

        if(error != true ) {

            var data = {
                'action': 'signup_check_login',
                'name': name,
                'email': email
            };

        }

        $.ajax({
            url: MyAjax.ajaxurl,
            type: 'POST',
            data: data,
            success: function (response) {
                console.log(data);
                data = JSON.parse(response);
                if (data.response !== 'false') {
                    $('.form-sign-up__title').text('Thank you! We will contact you shortly!');
                    $('#sgn_name').val("");
                    $('#sgn_email').val("");
                }
            }
        });
    });


// ------------------------------------------------------------------------------------------------------------


    // Check Get latest News
    $('body').on('click', '#sgn_submit_get' ,function(e){
        e.preventDefault();

        var error = false;
        var name  = $('#sgn_name').val();
        var email = $('#sgn_email').val();


        if( $('#sgn_name').val() == '' ) {
            $('#sgn_name').parent().css('border', '1px solid red');
            var error = true;
        } else {
            $('#sgn_name').parent().css('border', '0px solid #d0d2d3');
        }

        var email = $('#sgn_email').val();
        regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if(!regex.test(email)){
            $('#sgn_email').parent().css('border', '1px solid red');
            var error = true;
        } else {
            $('#sgn_email').parent().css('border', '0px solid #d0d2d3');
        }

        if(error != true ) {

            var data = {
                'action': 'signup_check_get_latest',
                'name': name,
                'email': email
            };

        }

        $.ajax({
            url: MyAjax.ajaxurl,
            type: 'POST',
            data: data,
            success: function (response) {
                console.log(data);
                data = JSON.parse(response);
                if (data.response !== 'false') {
                    $('.form-subscribe__title').text('We will contact you shortly!');
                    $('#sgn_name').val("");
                    $('#sgn_email').val("");
                }
            }
        });
    });


    // ------------------------------------------------------------------------------------------------------------

    // Check Get latest News
    $('body').on('click', '#sgn_stndrt' ,function(e){
        e.preventDefault();

        var error = false;
        var name  = $('#sgn_name').val();
        var email = $('#sgn_email').val();
        var phone = $('#sgn_phone').val();
        var message = $('#sgn_message').val();

        if( $('#sgn_name').val() == '' ) {
            $('#sgn_name').parent().css('border', '1px solid red');
            var error = true;
        } else {
            $('#sgn_name').parent().css('border', '0px solid #d0d2d3');
        }

        if( $('#sgn_phone').val() == '' ) {
            $('#sgn_phone').parent().css('border', '1px solid red');
            var error = true;
        } else {
            $('#sgn_phone').parent().css('border', '0px solid #d0d2d3');
        }

        if( $('#sgn_message').val() == '' ) {
            $('#sgn_message').parent().css('border', '1px solid red');
            var error = true;
        } else {
            $('#sgn_message').parent().css('border', '0px solid #d0d2d3');
        }

        regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if(!regex.test(email)){
            $('#sgn_email').parent().css('border', '1px solid red');
            var error = true;
        } else {
            $('#sgn_email').parent().css('border', '0px solid #d0d2d3');
        }

        if(error != true ) {

            var data = {
                'action': 'signup_check_standart',
                'name': name,
                'email': email,
                'phone': phone,
                'message': message
            };

        }

        $.ajax({
            url: MyAjax.ajaxurl,
            type: 'POST',
            data: data,
            success: function (response) {
                console.log(data);
                data = JSON.parse(response);
                if (data.response !== 'false') {
                    $('.contact-us-page__title').text('Thanks! We will contact you shortly!');
                    $('#sgn_name').val("");
                    $('#sgn_email').val("");
                    $('#sgn_phone').val("");
                    $('#sgn_message').val("");
                }
            }
        });
    });


});