<?php
/**
 * Robsons
 *
 * @package WordPress
 * @subpackage Robsons
 * @since 1.0
 */

if ( ! function_exists( 'setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */


function setup() {

    /**
     * Add default posts and comments RSS feed links to <head>.
     */
    add_theme_support( 'automatic-feed-links' );

    /**
     * Enable support for post thumbnails and featured images.
     */
    add_theme_support( 'post-thumbnails' );

    /**
     * Add support for two custom navigation menus.
     */
    register_nav_menus( array(
        'primary'   => __( 'Primary Menu', 'theme' ),
        'secondary' => __('Secondary Menu', 'theme' )
    ) );

    /**
     * Enable support for the following post formats:
     * aside, gallery, quote, image, and video
     */
    add_theme_support( 'post-formats', array ( 'aside', 'gallery', 'quote', 'image', 'video' ) );
}
endif;

add_action( 'after_setup_theme', 'setup' );


/**
 * Load CSS files
 */
function load_css() {

	wp_enqueue_style( 'stylesheet', get_stylesheet_uri() );
	wp_enqueue_style( 'gfonts', 'https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&subset=devanagari,latin-ext');
  	wp_enqueue_style( 'gfonts_two', 'https://fonts.googleapis.com/css?family=Cormorant:300,400,500,600,700&subset=cyrillic,cyrillic-ext,latin-ext');

}

add_action( 'wp_enqueue_scripts', 'load_css' );


/**
 * Load JS files
 */
function load_js() {

    wp_enqueue_script( 'newscript', get_template_directory_uri()."/assets/js/main.min.js");
}

add_action( 'wp_enqueue_scripts', 'load_js' );


/**
 * Load Ajax
 */
add_action( 'wp_enqueue_scripts', function(){

  wp_enqueue_script( 'contacts', get_template_directory_uri() . '/assets/js/contacts.js');
  wp_localize_script( 'contacts', 'MyAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

});


/**
 * Load Font Awesome
 */
function font_awesome() {

  if (!is_admin()) {
    wp_register_style('font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css');
    wp_enqueue_style('font-awesome');
  }
}

add_action('wp_enqueue_scripts', 'font_awesome');


/**
 * Create Breadcrumbs
 */
function breadcrumbs() {
  echo '<a class="breadcrumb__item" href="'.home_url().'" rel="nofollow">Main</a>';
  if (is_category() || is_single()) {

	echo "<span class='breadcrumb__item active'>";
		the_category(' &bull; ');
	echo "</span>";

	if (is_single()) {
	  echo "<span class='breadcrumb__item active'>";
	  	the_title();
	  echo "</span>";
	}

  } elseif (is_page()) {
	echo "<span class='breadcrumb__item active'>";
		echo the_title();
	echo "</span>";
  }
}


/**
 * ACF Footer Option page
 */
if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'menu_title'    => 'Theme Settings',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'      => true
    ));

  acf_add_options_page(array(
	'page_title'    => 'Header',
	'menu_title'    => 'Header',
	'parent_slug'   => 'theme-general-settings',
  ));

  acf_add_options_page(array(
	'page_title'    => 'Footer',
	'menu_title'    => 'Footer',
	'parent_slug'   => 'theme-general-settings',
  ));

  acf_add_options_page(array(
	'page_title'    => 'Preset Sections',
	'menu_title'    => 'Preset Sections',
	'parent_slug'   => 'theme-general-settings',
  ));

  acf_add_options_page(array(
	'page_title'    => 'Contact',
	'menu_title'    => 'Contact',
	'parent_slug'   => 'theme-general-settings',
  ));

  acf_add_options_page(array(
  'page_title'    => 'Schema',
  'menu_title'    => 'Schema',
  'parent_slug'   => 'theme-general-settings',
  ));

}


/**
 * ACF Remove <p>
 */
function my_acf_add_local_field_groups() {
  remove_filter('acf_the_content', 'wpautop' );
}

add_action('acf/init', 'my_acf_add_local_field_groups');


/**
 * SVG
 */
function my_myme_types($mime_types){
  $mime_types['svg'] = 'image/svg+xml';
  return $mime_types;
}
add_filter('upload_mimes', 'my_myme_types', 1, 1);


/**
 * Length
 */
function new_excerpt_length($length) {
  return 20;
}
add_filter('excerpt_length', 'new_excerpt_length');


/**
 * Register Sidebar
 */
if ( function_exists('register_sidebar') )
  register_sidebar(array('name'=>'Footer',
	'before_widget' => '<div class="footer1">',
	'after_widget' => '</div>',
	'before_title' => '<div class="title">',
	'after_title' => '</div>',
  ));


/**
 * Pagination
 */
function pagination($pages = '', $range = 4)
{
  $showitems = ($range * 2)+1;

  global $paged;
  if(empty($paged)) $paged = 1;

  if($pages == '')
  {
	global $wp_query;
	$pages = $wp_query->max_num_pages;
	if(!$pages)
	{
	  $pages = 1;
	}
  }

  if(1 != $pages)
  {
	echo "<ul class=\"pagination\">";
	if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
	if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";

	for ($i=1; $i <= $pages; $i++)
	{
	  if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
	  {
		echo ($paged == $i)? "<li class=\"current\">".$i."</li>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
	  }
	}

	if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";
	if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
	echo "</ul>\n";
  }
}



/**
 * Sign Up Home
 */
function signup_check_login_callback() {

  $subject = 'Robsons';
  //  $name = $_POST['name'];
  $email = $_POST['email'];
  $message =  get_field( 'sign_up_email_text_for_user','option' );
  $admin_email = get_option('admin_email');

  //  E-mail for admin:
  wp_mail($admin_email,'New User','TESTESTETSSETSE');

  //  E-mail for user:
  wp_mail($email, $subject, $message);

  echo json_encode($message);
  die;
}

add_action( 'wp_ajax_signup_check_login', 'signup_check_login_callback');
add_action( 'wp_ajax_nopriv_signup_check_login', 'signup_check_login_callback');


/**
 * Check Get latest News
 */
function signup_check_get_latest_callback() {

  $subject = 'Robsons';
  //  $name = $_POST['name'];
  $email = $_POST['email'];
  $message =  get_field( 'get_latest_news_email_text_for_user','option' );
  $admin_email = get_option('admin_email');

  //  E-mail for admin:
  wp_mail($admin_email,'Get latest News','Get latest NewsGet latest NewsGet latest News');

  //  E-mail for user:
  wp_mail($email, $subject, $message);

  echo json_encode($message);
  die;
}

add_action( 'wp_ajax_signup_check_get_latest', 'signup_check_get_latest_callback');
add_action( 'wp_ajax_nopriv_signup_check_get_latest', 'signup_check_get_latest_callback');


/**
 * Check Standart Contact Form
 */
function signup_check_standart_callback() {

  $subject = 'Robsons';
  //  $name = $_POST['name'];
  $email = $_POST['email'];
//  $phone = $_POST['phone'];
//  $message = $_POST['message'];

  $message =  get_field( 'standart_email_text_for_user','option' );
  $admin_email = get_option('admin_email');

  //  E-mail for admin:
  wp_mail($admin_email,'Standart Contact Form','Standart Contact FormStandart Contact FormStandart Contact Form');

  //  E-mail for user:
  wp_mail($email, $subject, $message);

  echo json_encode($message);
  die;
}

add_action( 'wp_ajax_signup_check_standart', 'signup_check_standart_callback');
add_action( 'wp_ajax_nopriv_signup_check_standart', 'signup_check_standart_callback');


?>