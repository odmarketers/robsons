<?php
/*
 * Template Name: Blog Individual
 * Template Post Type: post
 */

get_header();  ?>
<body>

<main class="posts_area">
  <section class="breadcrumb-section breadcrumb-section_blog">
	<div class="container">
	  <div class="wrapper">
		<div class="breadcrumb-section__title">Blog Single</div>
		<ul class="breadcrumb">
		  <li class="breadcrumb__item"><a href="/">Home</a></li>
		  <li class="breadcrumb__item"><span>Blog Single</span></li>
		</ul>
	  </div>
	</div>
  </section>
  <section class="blog-posts">
	<div class="container">
	  <div class="row">
		<div class="col-xl-9">
		  <div class="blog-posts__title"><?php the_title(); ?></div>
		  <div class="blog-posts__icon-wrap"><img class="blog-posts__icon" src="<?php the_post_thumbnail_url(); ?>" alt="" role="presentation" />
			<div class="blog-posts__block"><?php the_time('M d, Y') ?> | <span><span><?php the_category(', ') ?></span></div>

			<?php
			$next_post = get_next_post();
			if (!empty( $next_post )): ?>
                <a href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>" >
                    <div class="nav-arrows__left slick-arrow prev-tabs"><svg viewbox="90.838 84.5 430.324 617.5"><path d="M97.31,289.887c-8.629,8.337-8.629,22.214,0,30.843c8.338,8.337,22.214,8.337,30.532,0l156.391-156.392  v516.07c0.02,12.03,9.582,21.592,21.612,21.592s21.902-9.562,21.902-21.592v-516.07l156.101,156.392  c8.629,8.337,22.524,8.337,30.843,0c8.629-8.629,8.629-22.525,0-30.843L321.275,96.472c-8.337-8.629-22.213-8.629-30.532,0  L97.31,289.887z"></path></svg></div>
                </a>
            <?php endif; ?>

			<?php
			$previous_post = get_previous_post();
			if (!empty( $previous_post )): ?>
                <a href="<?php echo esc_url( get_permalink( $previous_post->ID ) ); ?>" >
                    <div class="nav-arrows__right slick-arrow next-tabs"><svg viewbox="90.838 84.5 430.324 617.5"><path d="M97.31,289.887c-8.629,8.337-8.629,22.214,0,30.843c8.338,8.337,22.214,8.337,30.532,0l156.391-156.392  v516.07c0.02,12.03,9.582,21.592,21.612,21.592s21.902-9.562,21.902-21.592v-516.07l156.101,156.392  c8.629,8.337,22.524,8.337,30.843,0c8.629-8.629,8.629-22.525,0-30.843L321.275,96.472c-8.337-8.629-22.213-8.629-30.532,0  L97.31,289.887z"></path></svg></div>
                </a>
			<?php endif; ?>

		  </div>
		  <?php the_post(); the_content(); ?>
		</div>
		<div class="col-xl-3">
		  <div class="active-block">
			<div class="active-block__icon"><img src="<?php echo get_template_directory_uri()?>/assets/img/active-block.png"></div><a class="button button_primary" href="#">Active button</a></div>
		  <form class="form-subscribe">
			<div class="form-subscribe__title">Get latest News</div>
			<div class="form-subscribe__sub-title"></div>
			<div class="form-subscribe__input-wrap">
                <input id="sgn_name" class="form-subscribe__input" placeholder="Your name" type="text" />
            </div>
			<div class="form-subscribe__input-wrap">
                <input id="sgn_email" class="form-subscribe__input" placeholder="E-mail" type="text" />
            </div>
              <button id="sgn_submit_get" class="button button_primary" type="button">Subscribe</button>
          </form>
		  <div class="instagram-section">
			<div class="instagram-section__title">Instagram</div>
			<div class="instagram-section__list">
                <a class="instagram-section__item" href="#">
                    <img src="<?php echo get_template_directory_uri()?>/assets/img/instagram-section.png"></a>
                <a class="instagram-section__item" href="#">
                    <img src="<?php echo get_template_directory_uri()?>/assets/img/instagram-section.png"></a>
                <a class="instagram-section__item" href="#">
                    <img src="<?php echo get_template_directory_uri()?>/assets/img/instagram-section.png"></a>
			  <a class="instagram-section__item" href="#">
                  <img src="<?php echo get_template_directory_uri()?>/assets/img/instagram-section.png"></a>
                <a class="instagram-section__item" href="#">
                    <img src="<?php echo get_template_directory_uri()?>/assets/img/instagram-section.png"></a>
                <a class="instagram-section__item" href="#">
                    <img src="<?php echo get_template_directory_uri()?>/assets/img/instagram-section.png"></a>
            </div>
		  </div>
		  <div class="instagram-section-post">
			<div class="instagram-section-post__title">Recent Posts</div>
			<div class="instagram-section-post__list">


			  <?php

			  $tags = wp_get_post_tags($post->ID);

			  if ($tags) {
				$first_tag = $tags[0]->term_id;
				$args = array(
				  'tag__in' => array($first_tag),
				  'post__not_in' => array($post->ID),
				  'posts_per_page'=>3,
				  'caller_get_posts'=>1
				);

				$my_query = new WP_Query($args);
				if( $my_query->have_posts() ) {
				  while ($my_query->have_posts()) : $my_query->the_post(); ?>

                      <div class="instagram-section-post__item">
                          <div class="instagram-section-post__top">
                              <a class="instagram-section-post__icon" href="<?php the_permalink() ?>">
                                  <img src="<?php the_post_thumbnail_url();?>">
                              </a>
                              <div class="instagram-section-post__date"><span><?php the_time('d m, Y') ?></span></div>
                          </div>
                          <a class="instagram-section-post__title-list" href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                      </div>

				  <?php
				  endwhile;
				}
				wp_reset_query();
			  }
			  ?>

			</div>
		  </div>
		  <div class="form-search">
              <form style="width: 66%;" class="search" method="get" action="<?php echo home_url(); ?>" role="search">
                  <input type="search" class="form-search__input" placeholder="<?php echo esc_attr_x( 'Enter Keyword', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
                  <button style="margin-top: -50px;" type="submit" role="button" class="form-search__button"/><i class="fa fa-search" aria-hidden="true"></i></button>
              </form>
          </div>
		  <div class="categories-post">
			<div class="categories-post__title">Categories</div>
              <div class="categories-post__list">
                  <style>

                      .categories-post__list a {
                          font-size: 14px;
                          color: #555555;
                          font-weight: 500;
                          font-family: "Poppins", sans-serif;
                          display: block;
                          margin-bottom: 20px;
                          padding-bottom: 15px;
                          border-bottom: 1px solid rgba(35, 43, 52, 0.1);
                          transition: all ease-in-out 0.2s; }
                      .categories-post__list a:last-child {
                          border: none;
                          margin-bottom: 0;
                          padding-bottom: 0; }
                      .categories-post__list a:hover {
                          color: #dfbc82; }

                  </style>

			<?php
			$args = array(
			  'show_option_all'    => '',
			  'show_option_none'   => __('No categories'),
			  'orderby'            => 'name',
			  'order'              => 'ASC',
			  'show_last_update'   => 0,
			  'style'              => '',
			  'show_count'         => 0,
			  'hide_empty'         => 1,
			  'use_desc_for_title' => 1,
			  'child_of'           => 0,
			  'hierarchical'       => true,
			  'title_li'           => __( 'Categories' ),
			  'number'             => 3,
			  'taxonomy'           => 'category',
			  'walker'             => 'Walker_Category',
			  'hide_title_if_empty' => false,
			  'separator'          => '<br />',
			);

			wp_list_categories( $args );

            ?>

<!--                <a class="categories-post__item" href="#">Diamonds</a>-->
<!--                <a class="categories-post__item" href="#">Trends</a>-->
<!--                <a class="categories-post__item" href="#">Jewelry</a>-->
            </div>
		  </div>
		</div>
	  </div>
	</div>
  </section>
  <section class="visit-our">
	<div class="container">
	  <div class="visit-our__title">Visit Our Showroom</div>
	  <div class="visit-our__sub-title">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal</div><a class="button button_primary" href="#">Schedule Appointment</a></div>
  </section>
    <!-- Maps -->
    <section class="maps">
        <div class="container">
            <div class="wrapper-info">
                <div class="wrapper-info__info">
                    <div class="wrapper-info__item">
                        <div class="wrapper-info__name"><?php the_field('map_block_1_title',option); ?></div>
                        <div class="wrapper-info__text"><?php the_field('map_block_1_text',option); ?></div>
                    </div>
                    <div class="wrapper-info__item">
                        <div class="wrapper-info__name"><?php the_field('map_block_3_title',option); ?></div>
                        <div class="wrapper-info__text"><a href="tel:<?php the_field('map_block_3_text',option); ?>"><?php the_field('map_block_3_text',option); ?></a></div>
                    </div>
                    <div class="wrapper-info__item">
                        <div class="wrapper-info__name"><?php the_field('map_block_2_title',option); ?></div>
                        <div class="wrapper-info__text"><a href="mailto:<?php the_field('map_block_2_text',option); ?>"><?php the_field('map_block_2_text',option); ?></a></div>
                    </div>
                </div>
                <div class="wrapper-info__last">
                    <p><?php the_field('map_bottom_text',option); ?></p><a class="button button_light" href="<?php the_field('map_bottom_url',option); ?>"><?php the_field('button_text',option); ?></a></div>
            </div>
        </div>
        <div id="map"></div>
        <script>
            var mapElement = document.getElementById('map');

            function mapinit() {
                var zoom = 18;
                var mapOptions = {
                    zoom: zoom,
                    disableDefaultUI: true,
                    center: new google.maps.LatLng(29.769992, -94.975720),
                    //scrollwheel: false
                    //marke: false
                };
                var map = new google.maps.Map(mapElement, mapOptions);
                var marker = new google.maps.Marker({
                    icon: {
                        url: 'assets/img/maps-filled-point.png',
                        anchor: new google.maps.Point(35, 60)
                    },
                    position: {
                        lat: 29.769992,
                        lng: -94.975720
                    },
                    map: map
                });
                if (window.innerWidth >= 1024) {
                    map.panBy(0, -100);
                }
                if (window.innerWidth < 1024) {
                    map.panBy(-60, -40);
                }
                if (window.innerWidth < 768) {
                    map.panBy(30, -20);
                }
            }
        </script>
        <script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD41ju8fEBULLIEGvSODoqTUIGcX5nQxA4&amp;callback=mapinit"></script>
    </section>
</main>

<!-- Footer -->
<?php get_footer(); ?>

</body>
</html>