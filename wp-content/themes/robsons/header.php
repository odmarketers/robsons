<?php
/**
 * The header for our theme
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">

          <?php
          if( have_rows('script_in_head',option) ):
            echo "<!-- Custom script start-->";
            while ( have_rows('script_in_head', option) ) : the_row();

              the_sub_field('script_head');

            endwhile;
			echo "<!-- Custom script end-->";
          else : endif;
          ?>

  <?php wp_head(); ?>
</head>

    <?php
        $menu_name = "Robsons Menu";
        $menu_obj = get_term_by( 'name', $menu_name, 'nav_menu' );
        $menu_id = $menu_obj->term_id;
        $menuitems = wp_get_nav_menu_items( $menu_id, array( 'order' => 'DESC' ) );
    ?>
      <header class="header">
        <div class="header-top">
            <div class="container">
                <div class="header-top__wrapper">
                    <ul class="social">
                        <li class="social__items"><a class="social__link" href="<?php the_field('header_google_url',option); ?>"><i class="fa fa-google-plus"></i></a></li>
                        <li class="social__items"><a class="social__link" href="<?php the_field('header_vimeo_url',option); ?>"><i class="fa fa-vimeo"></i></a></li>
                        <li class="social__items"><a class="social__link" href="<?php the_field('header_facebook_url',option); ?>"><i class="fa fa-facebook-f"></i></a></li>
                        <li class="social__items"><a class="social__link" href="<?php the_field('header_twitter_url',option); ?>"><i class="fa fa-twitter"></i></a></li>
                    </ul><a class="header-top__logo" href="/"><img src="<?php the_field('header_logo_image',option); ?>" alt=""/></a>
                    <div class="header-contacts"><a class="header-top__tel" href="tel:<?php the_field('header_phone',option); ?>call"> <i class="fa fa-phone"></i><?php the_field('header_phone',option); ?></a></div>
                </div>
            </div>
        </div>
          <div id="header-nav-items" class="header-nav">
              <div class="container">

				<?php
				    wp_nav_menu( array(
				            'theme_location' => 'primary',
                            'container' => 'div',
                            'menu_class' => 'header-nav__items',
                            'menu_id' => '4',
                            'echo' => true,
                            'fallback_cb' => 'wp_page_menu',
                            'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                            'depth' => 0, 'walker' => '',
				    ));
				?>

              </div>
          </div>
          <div class="header-mobile">
              <div class="container">
                  <div class="header-mobile__wrapper">
                      <a class="header-mobile__logo" href="/">
                          <img src="<?php the_field('mobile_header_logo',option); ?>" /></a>
                      <div class="header-mobile__button-mobile button-mobile-open">
                          <div class="icon-bar"></div>
                          <div class="icon-bar"></div>
                          <div class="icon-bar"></div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="header-mobile-nav">
              <div class="header-mobile-nav__wrapper">
                  <ul class="header-mobile-nav__items">

					<?php
					$count = 0;
					$submenu = false;
					foreach( $menuitems as $item ):
					  $link = $item->url;
					  $title = $item->title;
					  if ( !$item->menu_item_parent ):
						$parent_id = $item->ID;
						?>
                          <li class="header-mobile-nav__item"><a href="<?php echo $link; ?>"><span><?php echo $title; ?></span></a>
					  <?php endif; ?>
					  <?php if ( $parent_id == $item->menu_item_parent ): ?>
					  <?php if ( !$submenu ): $submenu = true; ?>
                            <ul class="header-mobile-nav__dropdown-mobile dropdown-mobile">
					  <?php endif; ?>
                        <li><a href="<?php echo $link; ?>" ><?php echo $title; ?></a></li>
					  <?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ): ?>
                            </ul>
						<?php $submenu = false; endif; ?>
					<?php endif; ?>
					  <?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id ): ?>
                        </li>
					  <?php $submenu = false; endif; ?>
					  <?php $count++; endforeach; ?>

                  </ul>
              </div>
          </div>
    </header>