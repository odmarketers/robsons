<?php
/**
 * Version       : 1.0
 * Author        : Nikita Titov
 *
 **/
?>

<!DOCTYPE html>
<?php get_header(); ?>
<body>

    <main class="posts_area">

      <!-- Basics Blocks -->

      <?php get_template_part( 'template-parts/content_blocks' ); ?>

    </main>

<?php get_footer(); ?>
</body>
</html>