<?php
/**
 * The footer for our theme
 */
?>

<?php ?>
    <footer class="footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="footer-logo col-xl-4 col-lg-4 col-md-12"><a href="/"><img src="<?php the_field('footer_image',option); ?>"/></a>
                        <div class="footer-logo__quote"><?php the_field('footer_text',option); ?></div>

                        <?php $select = get_field('do_we_use_social_icons',option); ?>

                        <?php if ( $select == "yes" ) { ?>

                            <ul class="social">
                                <li class="social__items"><a class="social__link" href="<?php the_field('header_google_url',option); ?>"><i class="fa fa-google-plus"></i></a></li>
                                <li class="social__items"><a class="social__link" href="<?php the_field('header_vimeo_url',option); ?>"><i class="fa fa-vimeo"></i></a></li>
                                <li class="social__items"><a class="social__link" href="<?php the_field('header_facebook_url',option); ?>"><i class="fa fa-facebook-f"></i></a></li>
                                <li class="social__items"><a class="social__link" href="<?php the_field('header_twitter_url',option); ?>"><i class="fa fa-twitter"></i></a></li>
                            </ul>

                        <?php } ?>

                        <form class="form-footer"><input class="form-footer__input" placeholder="Subscribe Newsletter" type="text" /><button class="button" type="submit"><i class="fa fa-paper-plane"></i></button></form>
                    </div>
                    <div class="footer-nav col-xl-3 offset-xl-1 col-lg-4 col-md-6">

					    <?php dynamic_sidebar( 'footer' ); ?>

                    </div>
                    <div class="footer-info col-xl-4 col-lg-4 col-md-6">
                        <div class="footer-info__item">
                            <div class="footer-info__title-info"> Address:</div>
                            <div class="footer-info__text-info"><?php the_field('footer_address',option); ?></div>
                        </div>
                        <div class="footer-info__item">
                            <div class="footer-info__title-info">Phone:</div>
                            <div class="footer-info__text-info"> <a href="tel: <?php the_field('address_phone',option); ?>call"><?php the_field('address_phone',option); ?>  </a></div>
                        </div>
                        <div class="footer-info__item">
                            <div class="footer-info__title-info">E-mail:</div><a class="footer-info__text-info footer-info__text-info_email" href="#"><?php the_field('footer_e-mail',option); ?></a></div>
                        <div class="footer-info__item">
                            <div class="footer-info__title-info hours">Hours of Operation</div>
                            <div class="footer-info__text-info footer-info__text-info_days"><?php the_field('footer_hours_of_operation',option); ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
			    <?php $check = get_field('show_copyright_2018',option); ?>
                    <div class="footer-bottom__copyright">
                        <?php if ( $check == "yes" ) { ?> Copyright <?php echo date('Y'); ?> © <?php } ?>
                            <?php the_field('company_name',option); ?>
                        <?php if ( $check == "yes" ) { ?>All rights reserved.<?php } ?>
                    </div>
            </div>
              <?php if( get_field('extra_text',option) ): ?>
                  <div class="container extra">
                    <?php the_field('extra_text',option); ?>
                  </div>
              <?php endif; ?>
        </div>
    </footer>

    <?php
    if( have_rows('scripts_in_footer',option) ):
      echo "<!-- Custom script start-->";
      while ( have_rows('scripts_in_footer', option) ) : the_row();

        the_sub_field('script');

      endwhile;
      echo "<!-- Custom script end-->";
    else : endif;
    ?>

<?php wp_footer(); ?>