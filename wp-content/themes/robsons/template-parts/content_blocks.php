<?php
/**
 * Template part for content
 * @package WordPress
 * @subpackage Robsons
 */
?>

<?php

  if( have_rows('content') ):
	while( have_rows('content') ) : the_row();

		if( get_row_layout() == 'visit_our_showroom' ):
		  $show = get_sub_field('show_hide');
		  if ($show == "1"): ?>
              <section class="visit-our">
                <div class="container">
                  <div class="visit-our__title"><?php the_sub_field('title'); ?></div>
                  <div class="visit-our__sub-title"><?php the_sub_field('description'); ?></div>
                  <a class="button button_primary" href="<?php  the_sub_field('button_url'); ?>"><?php the_sub_field('button_text'); ?></a></div>
              </section><?php
        endif;

		elseif( get_row_layout() == 'google_map' ):
		  $maps = get_sub_field('use_block_with_google_maps');
			if ($maps == "1"): ?>
				<section class="maps">
					<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13853.069791858557!2d-94.9757292!3d29.7699072!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4e94d8deadc5f13f!2sRobsons+Diamond+Jewelers!5e0!3m2!1suk!2sua!4v1527073823044" width="100%" height="550" frameborder="0" style="border:0" allowfullscreen></iframe>
					<div class="container">
					  <div class="wrapper-info">
						<div class="wrapper-info__info">
						  <div class="wrapper-info__item">
							<div class="wrapper-info__name"><?php the_field('map_block_1_title',option); ?></div>
				<div class="wrapper-info__text"><?php the_field('map_block_1_text',option); ?></div>
				</div>
				<div class="wrapper-info__item">
				  <div class="wrapper-info__name"><?php the_field('map_block_3_title',option); ?></div>
				  <div class="wrapper-info__text"><a href="tel:<?php the_field('map_block_3_text',option); ?>"><?php the_field('map_block_3_text',option); ?></a></div>
				</div>
				<div class="wrapper-info__item">
				  <div class="wrapper-info__name"><?php the_field('map_block_2_title',option); ?></div>
				  <div class="wrapper-info__text"><a href="mailto:<?php the_field('map_block_2_text',option); ?>"><?php the_field('map_block_2_text',option); ?></a></div>
				</div>
				</div>
				<div class="wrapper-info__last">
				  <p><?php the_field('map_bottom_text',option); ?></p><a class="button button_light" href="<?php the_field('map_bottom_url',option); ?>"><?php the_field('button_text',option); ?></a></div>
				</div>
				</div>
				<script>
					var mapElement = document.getElementById('map');

					function mapinit() {
						var zoom = 18;
						var mapOptions = {
							zoom: zoom,
							disableDefaultUI: true,
							center: new google.maps.LatLng(29.769992, -94.975720),
							//scrollwheel: false
							//marke: false
						};
						var map = new google.maps.Map(mapElement, mapOptions);
						var marker = new google.maps.Marker({
							icon: {
								url: 'http://woocommerce-158890-504939.cloudwaysapps.com/wp-content/themes/robsons/assets/img/maps-filled-point.png',
								anchor: new google.maps.Point(35, 60)
							},
							position: {
								lat: 29.769992,
								lng: -94.975720
							},
							map: map
						});
						if (window.innerWidth >= 1024) {
							map.panBy(0, -100);
						}
						if (window.innerWidth < 1024) {
							map.panBy(-60, -40);
						}
						if (window.innerWidth < 768) {
							map.panBy(30, -20);
						}
					}
				</script>
				<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD41ju8fEBULLIEGvSODoqTUIGcX5nQxA4&amp;callback=mapinit"></script>
				</section> <?php
			endif;

		elseif( get_row_layout() == 'testimonials' ):
		  $show = get_sub_field('show_hide');
		  if ($show == "1"): ?>

		<section class="testimonials testimonials_page">
		  <div class="container">
			<div class="testimonials__title"><?php the_sub_field('title'); ?></div>
			<div class="testimonials__sub-title"><?php the_sub_field('description'); ?></div>
			<div class="list-slide-testimonials">

			  <!-- SELECT FORM START -->
			  <?php $case = get_sub_field('test_contact_global_or_custom'); ?>

			  <?php if ($case == "global"): ?>

				<!-- contact Global-->
				<?php

				if( have_rows('global_testimonials',option) ):
				  while ( have_rows('global_testimonials',option) ) : the_row(); ?>

					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
					  <div class="list-slide-testimonials__item">
						<div class="list-slide-testimonials__icon" style="background-image: url(<?php the_sub_field('image',option); ?>);"></div>
						<div class="list-slide-testimonials__title"><?php the_sub_field('title',option); ?></div>
						<div class="list-slide-testimonials__text"><?php the_sub_field('text',option); ?></div>
						<div class="list-slide-testimonials__stars">
						  <i style="color:#dfbc82; margin: 0 2px;" class="fa fa-star" aria-hidden="true"></i>
						  <i style="color:#dfbc82; margin: 0 2px;" class="fa fa-star" aria-hidden="true"></i>
						  <i style="color:#dfbc82; margin: 0 2px;" class="fa fa-star" aria-hidden="true"></i>
						  <i style="color:#dfbc82; margin: 0 2px;" class="fa fa-star" aria-hidden="true"></i>
						  <i style="color:#dfbc82; margin: 0 2px;" class="fa fa-star" aria-hidden="true"></i>
						</div>
						<div class="list-slide-testimonials__name"><?php the_sub_field('name',option); ?></div>
						<svg width="22px" height="20px" viewbox="0 0 612 792"><path fill="#EDC77C" d="M580.563,125.859H389.791c-17.332,0-31.437,14.104-31.437,31.437v190.772                    c0,17.332,14.104,31.437,31.437,31.437h91.322c-1.195,49.964-12.79,89.888-34.783,120.009                    c-17.332,23.668-43.51,43.391-78.652,58.93c-16.137,7.052-23.069,26.177-15.539,42.074l22.592,47.693                    c7.291,15.3,25.341,21.994,40.88,15.3c41.597-17.93,76.619-40.641,105.307-68.252c34.903-33.708,58.81-71.719,71.719-114.152                    S612,380.819,612,307.308V157.296C612,139.964,597.896,125.859,580.563,125.859z"></path><path fill="#EDC77C" d="M66.579,663.392c40.999-17.93,75.902-40.641,104.709-68.133c35.262-33.708,59.288-71.6,72.197-113.675                    c12.909-42.074,19.364-100.167,19.364-174.157V157.296c0-17.332-14.104-31.437-31.437-31.437H40.641                    c-17.212,0-31.317,14.104-31.317,31.437v190.772c0,17.332,14.105,31.437,31.437,31.437h91.322                    c-1.195,49.964-12.79,89.888-34.784,120.009c-17.332,23.668-43.509,43.391-78.651,58.93C2.51,565.495-4.423,584.62,3.108,600.518                    l22.591,47.574C32.871,663.392,51.04,670.205,66.579,663.392z"></path></svg></div>
					</div>

				  <?php  endwhile;
				else :
				endif;

				?>

			  <?php endif; ?>

			  <?php if ($case == "custom"): ?>

				<!-- contact Custom -->
				<?php

				if( have_rows('testimonials_rep') ):
				  while ( have_rows('testimonials_rep') ) : the_row(); ?>

					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
					  <div class="list-slide-testimonials__item">
						<div class="list-slide-testimonials__icon" style="background-image: url(<?php the_sub_field('image'); ?>);"></div>
						<div class="list-slide-testimonials__title"><?php the_sub_field('title'); ?></div>
						<div class="list-slide-testimonials__text"><?php the_sub_field('text'); ?></div>
						<div class="list-slide-testimonials__stars">
						  <i style="color:#dfbc82; margin: 0 2px;" class="fa fa-star" aria-hidden="true"></i>
						  <i style="color:#dfbc82; margin: 0 2px;" class="fa fa-star" aria-hidden="true"></i>
						  <i style="color:#dfbc82; margin: 0 2px;" class="fa fa-star" aria-hidden="true"></i>
						  <i style="color:#dfbc82; margin: 0 2px;" class="fa fa-star" aria-hidden="true"></i>
						  <i style="color:#dfbc82; margin: 0 2px;" class="fa fa-star" aria-hidden="true"></i>
						</div>
						<div class="list-slide-testimonials__name"><?php the_sub_field('name'); ?></div>
						<svg width="22px" height="20px" viewbox="0 0 612 792"><path fill="#EDC77C" d="M580.563,125.859H389.791c-17.332,0-31.437,14.104-31.437,31.437v190.772                    c0,17.332,14.104,31.437,31.437,31.437h91.322c-1.195,49.964-12.79,89.888-34.783,120.009                    c-17.332,23.668-43.51,43.391-78.652,58.93c-16.137,7.052-23.069,26.177-15.539,42.074l22.592,47.693                    c7.291,15.3,25.341,21.994,40.88,15.3c41.597-17.93,76.619-40.641,105.307-68.252c34.903-33.708,58.81-71.719,71.719-114.152                    S612,380.819,612,307.308V157.296C612,139.964,597.896,125.859,580.563,125.859z"></path><path fill="#EDC77C" d="M66.579,663.392c40.999-17.93,75.902-40.641,104.709-68.133c35.262-33.708,59.288-71.6,72.197-113.675                    c12.909-42.074,19.364-100.167,19.364-174.157V157.296c0-17.332-14.104-31.437-31.437-31.437H40.641                    c-17.212,0-31.317,14.104-31.317,31.437v190.772c0,17.332,14.105,31.437,31.437,31.437h91.322                    c-1.195,49.964-12.79,89.888-34.784,120.009c-17.332,23.668-43.509,43.391-78.651,58.93C2.51,565.495-4.423,584.62,3.108,600.518                    l22.591,47.574C32.871,663.392,51.04,670.205,66.579,663.392z"></path></svg></div>
					</div>

				  <?php  endwhile;
				else :
				endif;
				?>

			  <?php endif; ?>
			  <!-- SELECT FORM END -->

			</div><a class="button button_light" href="<?php the_sub_field('button_url'); ?>"><?php the_sub_field('button_text'); ?></a></div>
		</section> <?php
        endif;

		elseif( get_row_layout() == 'faq' ):
            $show = get_sub_field('show_hide');
            if ($show == "1"): ?>
		  <section class="faq-section faq-section_pages">
			<div class="container">
			  <div class="faq-section__title"><?php the_sub_field('title'); ?></div>
			  <div class="list-faq col-xl-8 offset-xl-2 js-accordion">

				<?php
				if( have_rows('faq_repeater') ):
				  while ( have_rows('faq_repeater') ) : the_row(); ?>

					<div class="list-faq__header js-accordion-header"><span><?php the_sub_field('question'); ?><div class="svg"><svg viewbox="0 0 320 512"><path d="M143 352.3L7 216.3c-9.4-9.4-9.4-24.6 0-33.9l22.6-22.6c9.4-9.4 24.6-9.4 33.9 0l96.4 96.4 96.4-96.4c9.4-9.4 24.6-9.4 33.9 0l22.6 22.6c9.4 9.4 9.4 24.6 0 33.9l-136 136c-9.2 9.4-24.4 9.4-33.8 0z"></path></svg></div></span></div>
					<div class="list-faq__content js-accordion-content">
					  <p><?php the_sub_field('answer'); ?></p>
					</div>

				  <?php  endwhile;
				else :
				endif;
				?>

			  </div>
			</div>
		  </section> <?php
          endif;

        elseif( get_row_layout() == 'get_your_free_insurance_quote_now' ):
                $show = get_sub_field('show_hide');
                if ($show == "1"): ?>
                    <section style="background-image: url(<?php the_sub_field('background_image'); ?>);" class="free-insurance" >
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-10 offset-xl-1 col-lg-10 offset-lg-1">
                                    <div class="free-insurance__wrapper">
                                        <div class="free-insurance__content">
                                            <div class="free-insurance__title"><?php the_sub_field('title'); ?></div>
                                            <div class="free-insurance__sub-title"><?php the_sub_field('description'); ?></div>
                                            <a class="button button_primary" href="<?php the_sub_field('button_url'); ?>"><?php the_sub_field('button_text'); ?></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section><?php
                endif;

        elseif( get_row_layout() == 'heres_how_it_all_works' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>

        <section class="here-how-it-all-works">
            <div class="container">
                <div class="here-how-it-all-works__title"><?php the_sub_field('title'); ?></div>
                <div class="list-all-works row">
                    <div class="col-xl-6 col-lg-6 col-md-6">
                        <div class="list-all-works__item">
                            <div class="list-all-works__top">
                                <div class="list-all-works__svg">
                                    <svg viewbox="76.8 128 614.4 768"><path d="M576,448H256c-7.065,0-12.8,5.722-12.8,12.8s5.734,12.8,12.8,12.8h320c7.065,0,12.8-5.722,12.8-12.8S583.065,448,576,448z"></path><path d="M256,371.2h128c7.065,0,12.8-5.722,12.8-12.8c0-7.078-5.734-12.8-12.8-12.8H256c-7.065,0-12.8,5.722-12.8,12.8   C243.2,365.479,248.935,371.2,256,371.2z"></path><path d="M576,550.4H256c-7.065,0-12.8,5.721-12.8,12.8c0,7.078,5.734,12.8,12.8,12.8h320c7.065,0,12.8-5.722,12.8-12.8   C588.8,556.121,583.065,550.4,576,550.4z"></path><path d="M576,652.8H256c-7.065,0-12.8,5.722-12.8,12.8c0,7.079,5.734,12.801,12.8,12.801h320c7.065,0,12.8-5.722,12.8-12.801   C588.8,658.521,583.065,652.8,576,652.8z"></path><path d="M576,755.2H256c-7.065,0-12.8,5.722-12.8,12.8s5.734,12.8,12.8,12.8h320c7.065,0,12.8-5.722,12.8-12.8   S583.065,755.2,576,755.2z"></path><path d="M627.2,314.701V128H76.8v704h64v64h550.4V378.701L627.2,314.701z M512,235.699l115.2,115.2l20.301,20.301H512V235.699z    M102.4,806.4V153.6h499.2v135.5L504.499,192H140.8v614.4H102.4z M166.4,870.4V832V217.6h320v179.2H665.6V870.4H166.4z"></path></svg></div>
                                <div class="list-all-works__title"><?php the_sub_field('title_1'); ?></div>
                            </div>
                            <p><?php the_sub_field('text_1'); ?></p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6">
                        <div class="list-all-works__item">
                            <div class="list-all-works__top">
                                <div class="list-all-works__svg"><svg viewbox="89.6 128 588.8 768"><path d="M640,819.2h-64v-76.8c0-97.165-53.082-184.832-137.139-230.4C522.918,466.432,576,378.765,576,281.6v-76.8h64   c21.171,0,38.4-17.229,38.4-38.4c0-21.171-17.229-38.4-38.4-38.4H128c-21.171,0-38.4,17.229-38.4,38.4   c0,21.171,17.229,38.4,38.4,38.4h64v76.8c0,97.165,53.082,184.832,137.14,230.4C245.082,557.568,192,645.235,192,742.4v76.8h-64   c-21.171,0-38.4,17.229-38.4,38.399c0,21.172,17.229,38.4,38.4,38.4h512c21.171,0,38.4-17.229,38.4-38.4   C678.4,836.429,661.171,819.2,640,819.2z M128,179.2c-7.053,0-12.8-5.734-12.8-12.8c0-7.065,5.747-12.8,12.8-12.8h512   c7.053,0,12.8,5.734,12.8,12.8c0,7.066-5.747,12.8-12.8,12.8h-64H192H128z M217.6,281.6v-76.8h332.8v76.8   c0,95.065-56.205,180.199-143.398,217.6h-46.017C273.805,461.799,217.6,376.666,217.6,281.6z M217.6,742.4   c0-95.066,56.205-180.199,143.398-217.601h46.017C494.195,562.201,550.4,647.334,550.4,742.4v76.8c0-7.065-5.734-12.8-12.801-12.8   c-7.065,0-12.8,5.734-12.8,12.8h-25.6c0-7.065-5.734-12.8-12.8-12.8c-7.066,0-12.801,5.734-12.801,12.8H448   c0-7.065-5.734-12.8-12.8-12.8s-12.8,5.734-12.8,12.8H396.8c0-7.065-5.734-12.8-12.8-12.8s-12.8,5.734-12.8,12.8H345.6   c0-7.065-5.734-12.8-12.8-12.8s-12.8,5.734-12.8,12.8h-25.6c0-7.065-5.734-12.8-12.8-12.8c-7.066,0-12.8,5.734-12.8,12.8h-25.6   c0-7.065-5.734-12.8-12.8-12.8c-7.065,0-12.8,5.734-12.8,12.8V742.4z M640,870.4H128c-7.053,0-12.8-5.734-12.8-12.801   c0-7.065,5.747-12.8,12.8-12.8h64h384h64c7.053,0,12.8,5.734,12.8,12.8C652.8,864.666,647.053,870.4,640,870.4z"></path><circle cx="256" cy="793.6" r="12.8"></circle><circle cx="307.2" cy="793.6" r="12.8"></circle><circle cx="358.4" cy="793.6" r="12.8"></circle><circle cx="409.6" cy="793.6" r="12.8"></circle><circle cx="460.8" cy="793.6" r="12.8"></circle><circle cx="512" cy="793.6" r="12.8"></circle><circle cx="281.6" cy="768" r="12.8"></circle><circle cx="332.8" cy="768" r="12.8"></circle><circle cx="384" cy="550.4" r="12.8"></circle><circle cx="307.2" cy="409.6" r="12.8"></circle><circle cx="358.4" cy="409.6" r="12.8"></circle><circle cx="409.6" cy="409.6" r="12.8"></circle><circle cx="460.8" cy="409.6" r="12.8"></circle><circle cx="435.2" cy="384" r="12.8"></circle><circle cx="460.8" cy="358.4" r="12.8"></circle><circle cx="332.8" cy="384" r="12.8"></circle><circle cx="384" cy="384" r="12.8"></circle><circle cx="307.2" cy="358.4" r="12.8"></circle><circle cx="332.8" cy="435.2" r="12.8"></circle><circle cx="358.4" cy="460.8" r="12.8"></circle><circle cx="409.6" cy="460.8" r="12.8"></circle><circle cx="384" cy="435.2" r="12.8"></circle><circle cx="435.2" cy="435.2" r="12.8"></circle><circle cx="384" cy="588.8" r="12.8"></circle><circle cx="384" cy="627.2" r="12.8"></circle><circle cx="384" cy="665.6" r="12.8"></circle><circle cx="384" cy="704" r="12.8"></circle><circle cx="384" cy="742.4" r="12.8"></circle><circle cx="435.2" cy="768" r="12.8"></circle><circle cx="486.4" cy="768" r="12.8"></circle></svg></div>
                                <div class="list-all-works__title"><?php the_sub_field('title_2'); ?></div>
                            </div>
                            <p><?php the_sub_field('text_2'); ?></p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6">
                        <div class="list-all-works__item">
                            <div class="list-all-works__top">
                                <div class="list-all-works__svg"><svg viewbox="48.524 129.8 671.129 765.601"><path d="M648.9,297.05c-0.9-5.55-5.551-9.45-11.101-9.45c-105.899,0-246.6-83.7-247.95-84.6    c-3.6-2.1-8.1-2.1-11.55,0c-1.35,0.9-141.9,84.6-247.95,84.6c-5.55,0-10.2,4.05-11.1,9.45c-0.15,1.35-5.4,34.8-5.25,85.2    c0,6.15,5.1,11.25,11.25,11.25l0,0c6.3,0,11.25-5.1,11.25-11.25c0-33.3,2.4-59.25,3.9-72.3c45.45-1.95,100.2-16.65,162.9-43.8    c38.7-16.8,68.851-33.45,81-40.35c12.15,6.9,42.3,23.55,81,40.35C528,293.3,582.75,308,628.2,309.95    c3.149,27.9,10.35,115.05-10.65,209.1c-1.35,6.15,2.4,12.15,8.55,13.5c0.75,0.15,1.65,0.3,2.4,0.3c5.1,0,9.75-3.6,10.95-8.85    C665.55,405.95,649.65,301.55,648.9,297.05z"></path><path d="M621.9,547.55c-6-1.8-12.301,1.65-14.101,7.5c-18.3,61.05-46.95,112.65-85.2,153.15    c-37.05,39.45-83.699,69-138.75,87.899c-85.2-29.25-149.7-83.699-191.55-162c-31.35-58.5-49.8-129-54.75-209.55    c-0.45-6.149-5.7-10.95-12-10.5c-6.15,0.45-10.95,5.7-10.5,12c5.25,83.7,24.6,157.351,57.45,218.7    c45.3,84.6,115.2,143.25,207.9,174c1.199,0.45,2.399,0.6,3.6,0.6s2.4-0.149,3.6-0.6c60.15-19.95,111.15-51.9,151.5-95.1    c40.5-43.2,70.95-97.65,90.301-162.15C631.2,555.65,627.9,549.35,621.9,547.55z"></path><path d="M712.95,245.45c-0.9-5.55-5.55-9.45-11.101-9.45c-133.5,0-310.199-105.15-312-106.2    c-3.6-2.1-8.1-2.1-11.55,0c-1.8,1.05-178.5,106.2-312,106.2c-5.55,0-10.2,4.05-11.1,9.45c-0.75,5.25-19.8,130.05,10.05,273.3    C82.95,603.35,114,676.4,157.8,736.1C212.7,810.95,287.7,864.65,380.55,895.4c1.2,0.449,2.4,0.6,3.601,0.6    c1.199,0,2.399-0.15,3.6-0.6C480.6,864.5,555.6,810.95,610.5,736.1c43.8-59.699,75-132.75,92.55-217.35    C732.75,375.5,713.7,250.7,712.95,245.45z M680.85,514.1C663.9,595.4,634.05,665.6,592.2,722.75    C540.75,792.8,470.7,843.35,384,872.9c-86.55-29.551-156.6-79.95-207.9-149.851c-41.85-57-71.7-127.05-88.8-208.2    C62.7,397.4,72.3,290,76.05,258.35c57-1.95,126-20.25,205.35-54.75c49.95-21.6,88.5-43.05,102.6-51.15    c14.1,8.1,52.65,29.55,102.6,51.15c79.351,34.35,148.351,52.8,205.351,54.75C695.7,289.85,705.3,396.8,680.85,514.1z"></path><path d="M393.75,453.35l58.5-54.3l0.15-0.149c0,0,0,0,0.149-0.15c0.15-0.15,0.3-0.3,0.3-0.45l0,0    c0.15-0.149,0.15-0.3,0.301-0.45l0,0l0,0c0.149-0.149,0.149-0.3,0.3-0.6c0,0,0,0,0-0.15c0-0.149,0.149-0.3,0.149-0.449    c0,0,0,0,0-0.15s0-0.3,0.15-0.6v-0.15c0-0.15,0-0.3,0-0.45l0,0c0-0.15,0-0.3,0-0.6v-0.15c0-0.3-0.15-0.75-0.3-1.05v-0.15    c-0.15-0.15-0.15-0.3-0.3-0.45l-25.65-37.65c0,0,0-0.15-0.15-0.15c0-0.15-0.149-0.15-0.149-0.3l-0.15-0.15    c-0.149-0.15-0.149-0.15-0.3-0.3l-0.15-0.15c-0.149-0.15-0.149-0.15-0.3-0.3c0,0-0.149,0-0.149-0.15    c-0.15-0.15-0.301-0.15-0.45-0.3l0,0c-0.601-0.3-1.351-0.6-2.101-0.6h-82.05c-0.149,0-0.45,0-0.6,0c-1.351,0.15-2.55,0.9-3.3,2.1    l-25.351,37.8c-0.149,0.15-0.149,0.3-0.3,0.45v0.15c-0.15,0.3-0.3,0.75-0.3,1.05v0.15c0,0.15,0,0.3,0,0.6l0,0c0,0.15,0,0.3,0,0.45    v0.15c0,0.15,0,0.3,0.149,0.6c0,0,0,0,0,0.15s0.15,0.3,0.15,0.45c0,0,0,0,0,0.149c0,0.15,0.15,0.301,0.3,0.601l0,0l0,0    c0.15,0.149,0.15,0.3,0.3,0.45l0,0c0.15,0.149,0.301,0.3,0.301,0.449c0,0,0,0,0.149,0.15l0.15,0.15l58.5,54.3    c-47.101,5.55-83.7,45.75-83.7,94.2c0,52.35,42.6,94.949,94.95,94.949c52.35,0,94.95-42.6,94.95-94.949    C477.45,499.1,440.7,458.9,393.75,453.35z M392.85,441.35L406.5,400.4h30.45L392.85,441.35z M382.5,442.4l-13.95-42.15h28.05    L382.5,442.4z M424.05,366.95l16.05,24H411L424.05,366.95z M415.65,362.6l-12.601,23.25l-12.6-23.25H415.65z M395.1,390.95H369.9    l12.6-23.25L395.1,390.95z M374.55,362.6l-12.6,23.25L349.35,362.6C349.5,362.6,374.55,362.6,374.55,362.6z M341.1,366.8    l13.051,24H324.9L341.1,366.8z M328.05,400.25h30.601l13.649,41.1L328.05,400.25z M382.5,633.05c-47.1,0-85.5-38.399-85.5-85.5    c0-47.1,38.4-85.5,85.5-85.5s85.5,38.4,85.5,85.5C468,594.65,429.6,633.05,382.5,633.05z"></path><path d="M429.75,436.85c-2.4-1.05-5.1,0.15-6.15,2.551c-1.05,2.399,0.15,5.1,2.551,6.149    c40.949,17.55,67.35,57.601,67.35,102c0,61.2-49.8,110.851-110.85,110.851C321.6,658.4,271.8,608.6,271.8,547.55    c0-44.55,26.55-84.6,67.5-102.149c2.4-1.051,3.45-3.75,2.55-6.15c-1.05-2.4-3.75-3.45-6.149-2.55    c-44.55,18.899-73.35,62.399-73.35,110.7c0,66.3,54,120.3,120.3,120.3c66.3,0,120.3-54,120.3-120.3    C502.8,499.25,474.15,455.9,429.75,436.85z"></path></svg></div>
                                <div class="list-all-works__title"><?php the_sub_field('title_3'); ?></div>
                            </div>
                            <p><?php the_sub_field('text_3'); ?></p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6">
                        <div class="list-all-works__item">
                            <div class="list-all-works__top">
                                <div class="list-all-works__svg"><svg viewbox="0 195.592 768 632.816"><path d="M702.316,360.992H65.842C29.581,360.992,0,390.574,0,426.835v335.731c0,36.42,29.581,65.842,65.842,65.842h636.316    c36.42,0,65.842-29.581,65.842-65.842V426.835C768.159,390.574,738.578,360.992,702.316,360.992z M31.808,427.471    c0.318-19.085,15.586-34.512,34.671-34.67h81.428v16.54c0,52.643-42.304,96.378-94.946,96.378H31.808V427.471z M147.906,782.128    v14.632H66.479c-18.767,0-34.671-14.632-34.671-33.398v-76.339H52.96c52.483,0,94.946,42.464,94.946,94.787    C147.906,781.969,147.906,782.128,147.906,782.128z M737.941,763.361c0,18.767-15.903,33.398-34.67,33.398h-81.429v-14.473    c-0.159-52.483,42.146-95.105,94.629-95.265c0.159,0,0.317,0,0.317,0h21.152V763.361z M737.941,655.215h-21.152    c-69.977,0-126.754,56.777-126.754,126.754c0,0.159,0,0.159,0,0.318v14.473H179.714v-14.473    c0.159-69.978-56.459-126.913-126.436-127.072c-0.159,0-0.159,0-0.318,0H31.808V537.525H52.96    c70.136,0,126.754-57.89,126.754-128.186V392.8h410.321v15.745c0,70.137,56.618,127.391,126.754,127.391h21.152V655.215z     M737.941,504.128h-21.152c-52.642-0.159-95.105-42.782-94.946-95.424v-0.159V392.8h81.429    c19.084,0.318,34.511,15.585,34.67,34.67V504.128z"></path><path d="M384.875,418.405c-97.81,0-177.011,79.202-177.011,177.011c0,97.81,79.201,177.011,177.011,177.011    c97.809,0,177.01-79.201,177.01-177.011C561.727,497.766,482.524,418.564,384.875,418.405z M384.875,740.619    c-80.156,0-145.203-65.047-145.203-145.203c0-80.155,65.047-145.203,145.203-145.203c80.155,0,145.202,65.048,145.202,145.203    C529.918,675.571,465.03,740.46,384.875,740.619z"></path><path d="M669.873,278.292H99.876c-8.747,0-15.904,7.157-15.904,15.904S91.129,310.1,99.876,310.1h569.997    c8.747,0,15.903-7.157,15.903-15.904S678.62,278.292,669.873,278.292z"></path><path d="M586.536,195.592H183.213c-8.747,0-15.904,7.157-15.904,15.904s7.157,15.904,15.904,15.904h403.323    c8.747,0,15.904-7.157,15.904-15.904S595.283,195.592,586.536,195.592z"></path><path d="M384.875,574.582c-14.473,0.159-26.4-11.61-26.4-26.083c-0.159-14.472,11.609-26.4,26.082-26.4c0,0,0.159,0,0.318,0    c14.473,0,26.241,11.77,26.241,26.242c0,8.747,7.157,15.903,15.904,15.903s15.903-7.156,15.903-15.903    c0-25.924-17.176-48.666-42.146-55.823v-5.407c0-8.747-7.156-15.904-15.903-15.904s-15.904,7.157-15.904,15.904v5.407    c-30.854,8.747-48.666,40.873-39.919,71.727c7.157,24.97,29.899,42.146,55.823,42.146c14.473,0,26.241,11.769,26.241,26.241    s-11.769,26.242-26.241,26.242s-26.242-11.77-26.242-26.242c0-8.747-7.156-15.903-15.903-15.903s-15.904,7.156-15.904,15.903    c0,25.924,17.177,48.666,42.146,55.823v6.839c0,8.747,7.157,15.903,15.904,15.903s15.903-7.156,15.903-15.903v-6.839    c30.854-8.747,48.666-40.873,39.919-71.727C433.541,591.758,410.798,574.582,384.875,574.582z"></path></svg></div>
                                <div class="list-all-works__title"><?php the_sub_field('title_4'); ?></div>
                            </div>
                            <p><?php the_sub_field('text_4'); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </section><?php
        endif;

        elseif( get_row_layout() == 'breadcrumb' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="breadcrumb-section breadcrumb-section_insurance" style="background-image: url(<?php the_sub_field('background_image'); ?>);">
            <div class="container">
                <div class="wrapper">
                    <div class="breadcrumb-section__title"><?php the_sub_field('text'); ?></div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb__item"><a href="/">Home</a></li>
                        <li class="breadcrumb__item"><span><?php the_sub_field('text'); ?></span></li>
                    </ul>
                </div>
            </div>
            </section><?php
        endif;

        elseif( get_row_layout() == 'our-process-fourth' ):
        $maps = get_sub_field('show_hide');
        if ($maps == "1"): ?>
              <section class="our-process-fourth">
                <div class="container">
                  <div class="our-process-fourth__title"><?php the_sub_field('title'); ?></div>
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12">
                    <div class="our-process-fourth__img"><img src="<?php the_sub_field('image'); ?>"></div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12">
                    <div class="our-process-fourth__title-list"><?php the_sub_field('title_1'); ?></div>
                    <div class="our-process-fourth__month"><?php the_sub_field('description'); ?></div>
                    <p><?php the_sub_field('text_1'); ?></p>
                    <div class="our-process-fourth__title-list"><?php the_sub_field('title_2'); ?></div>
                  <?php the_sub_field('text_2'); ?>
                </div>
            </div>
            </div>
            </section><?php
        endif;

        elseif( get_row_layout() == 'before_after' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="before-after-bepairs-two">
                <div class="container">
                    <div class="before-after-bepairs-two__title"><?php the_sub_field('title'); ?></div>
                    <div class="before-after-bepairs-two__list row align-items-center">
                        <div class="col-xl-6">
                            <div class="before-after-bepairs-two__item">
                                <div class="before-after-bepairs-two__wrap col-xl-6">
                                    <div class="before-after-bepairs-two__icon"><img class="before-after-bepairs-two__img" src="<?php the_sub_field('before_1'); ?>" alt="" role="presentation" /></div>
                                </div>
                                <div class="before-after-bepairs-two__wrap col-xl-6">
                                    <div class="before-after-bepairs-two__icon"><img class="before-after-bepairs-two__img" src="<?php the_sub_field('after_1'); ?>" alt="" role="presentation" /></div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="before-after-bepairs-two__title-list">Before</div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="before-after-bepairs-two__title-list">After</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="before-after-bepairs-two__item">
                                <div class="before-after-bepairs-two__wrap col-xl-6">
                                    <div class="before-after-bepairs-two__icon"><img class="before-after-bepairs-two__img" src="<?php the_sub_field('before_2'); ?>" alt="" role="presentation" /></div>
                                </div>
                                <div class="before-after-bepairs-two__wrap col-xl-6">
                                    <div class="before-after-bepairs-two__icon"><img class="before-after-bepairs-two__img" src="<?php the_sub_field('after_2'); ?>" alt="" role="presentation" /></div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="before-after-bepairs-two__title-list">Before</div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="before-after-bepairs-two__title-list">After</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="before-after-bepairs-two__item">
                                <div class="before-after-bepairs-two__wrap col-xl-6">
                                    <div class="before-after-bepairs-two__icon"><img class="before-after-bepairs-two__img" src="<?php the_sub_field('before_3'); ?>" alt="" role="presentation" /></div>
                                </div>
                                <div class="before-after-bepairs-two__wrap col-xl-6">
                                    <div class="before-after-bepairs-two__icon"><img class="before-after-bepairs-two__img" src="<?php the_sub_field('after_3'); ?>" alt="" role="presentation" /></div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="before-after-bepairs-two__title-list">Before</div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="before-after-bepairs-two__title-list">After</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="before-after-bepairs-two__item">
                                <div class="before-after-bepairs-two__wrap col-xl-6">
                                    <div class="before-after-bepairs-two__icon"><img class="before-after-bepairs-two__img" src="<?php the_sub_field('before_4'); ?>" alt="" role="presentation" /></div>
                                </div>
                                <div class="before-after-bepairs-two__wrap col-xl-6">
                                    <div class="before-after-bepairs-two__icon"><img class="before-after-bepairs-two__img" src="<?php the_sub_field('after_4'); ?>" alt="" role="presentation" /></div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="before-after-bepairs-two__title-list">Before</div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="before-after-bepairs-two__title-list">After</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


        <?php endif;

        elseif( get_row_layout() == 'laser_engraving_center' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>

            <section class="watch-repair-center">
                <div class="container">
                    <div class="watch-repair-center__title"><?php the_sub_field('title'); ?></div>
                    <div class="row align-items-center">
                        <div class="col-xl-6">
                            <div class="watch-repair-center__icon"><img src="<?php the_sub_field('image'); ?>"></div>
                        </div>
                        <div class="watch-repair-center__text col-xl-6">
                          <?php the_sub_field('text'); ?>
                        </div>
                    </div>
                </div>
            </section> <?php

        endif;

        elseif( get_row_layout() == 'sell_my_jewelry_form' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="section-sell">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-10 offset-xl-1 col-lg-10 offset-lg-1">
                            <div class="form-section-sell">
                                <div class="form-section-sell__wrapper">
                                    <div class="form-section-sell__title">Sell My Jewelry</div>
                                    <div class="form-section-sell__sub-title">Please take just a moment to fill out this form and we will call you back with a tentative quote. Please note that due to the circumstances of not being able to test the gold and diamonds for authenticity, this is only a tentative
                                        quote.</div>
                                    <form class="form-section-sell-section">
                                        <div class="form-section-sell-section__input-wrap">
                                            <input class="form-section-sell-section__input" placeholder="Your name" type="text" />
                                            <div class="form-section-sell-section__icon"><svg viewbox="83.606 121.611 599.97 774.54"><path d="M384,503.6c85.35,0,154.65-84.149,154.65-187.8C538.65,212.15,515.85,128,384,128   c-131.85,0-154.65,84.15-154.65,187.8C229.35,419.45,298.65,503.6,384,503.6z"></path><path d="M91.95,790.4C91.95,784.1,91.95,788.6,91.95,790.4L91.95,790.4z"></path><path d="M676.05,795.35C676.05,793.55,676.05,783.35,676.05,795.35L676.05,795.35z"></path><path d="M675.75,782.75c-2.85-180.75-26.4-232.2-207-264.75c0,0-25.35,32.4-84.6,32.4S299.4,518,299.4,518   c-178.65,32.25-203.7,82.95-207,258.9c-0.3,14.399-0.45,15.149-0.45,13.5c0,3.149,0,8.85,0,19.05c0,0,43.05,86.7,292.05,86.7   s292.05-86.7,292.05-86.7c0-6.45,0-10.95,0-14.101C676.05,796.25,675.9,794.3,675.75,782.75z"></path></svg></div>
                                        </div>
                                        <div class="form-section-sell-section__input-wrap">
                                            <input class="form-section-sell-section__input" placeholder="Your phone" type="text" />
                                            <div class="form-section-sell-section__icon"><svg viewbox="0 0 512 512"><path d="M493.4 24.6l-104-24c-11.3-2.6-22.9 3.3-27.5 13.9l-48 112c-4.2 9.8-1.4 21.3 6.9 28l60.6 49.6c-36 76.7-98.9 140.5-177.2 177.2l-49.6-60.6c-6.8-8.3-18.2-11.1-28-6.9l-112 48C3.9 366.5-2 378.1.6 389.4l24 104C27.1 504.2 36.7 512 48 512c256.1 0 464-207.5 464-464 0-11.2-7.7-20.9-18.6-23.4z"></path></svg></div>
                                        </div>
                                        <div class="form-section-sell-section__input-wrap">
                                            <input class="form-section-sell-section__input" placeholder="Your email" type="text" />
                                            <div class="form-section-sell-section__icon"><svg viewbox="0 177.497 612 437.006"><path d="M598.135,189.331c-7.89-7.292-18.408-11.833-30.242-11.833H43.987c-11.714,0-22.232,4.542-30.122,11.953    L306,439.748L598.135,189.331z"></path><path d="M306,483.377l-75.663-66.22L13.866,602.67c7.889,7.291,18.527,11.833,30.241,11.833h523.906    c11.714,0,22.232-4.542,30.122-11.833L381.783,417.157L306,483.377z"></path><polygon points="0,215.866 0,579.002 211.212,399.467"></polygon><polygon points="400.788,399.467 612,579.002 612,215.627"></polygon></svg></div>
                                        </div>
                                        <div class="form-section-sell-section__label">
                                            <span>What are you selling?  </span>
                                            <div class="form-section-sell-section__input-wrap form-section-sell-section__input-wrap_margin">
                                                <select class="select-box">
                                                    <option>Gold</option>
                                                    <option>test 2</option>
                                                    <option>test 3</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-section-sell-section__label"><span>Do you want Cash or Store Credit (30% more value for store credit)</span>
                                            <div class="wrap">
                                                <label class="checkbox"><input type="radio" name="order" value="0">
                                                    <div class="icon"></div>Cash</label>
                                                <label class="checkbox"><input type="radio" name="order" value="1">
                                                    <div class="icon"></div>Store Credit</label>
                                            </div>
                                        </div>
                                        <div class="form-section-sell-section__label"><span>Selling Gold? How many grams?</span>
                                            <div class="form-section-sell-section__input-wrap form-section-sell-section__input-wrap_margin">
                                                <input class="form-section-sell-section__input form-section-sell-section__input_small" type="text" /></div>
                                        </div><a class="form-section-sell-section__download" href="#"><svg viewbox="0 0 512 512"><path d="M296 384h-80c-13.3 0-24-10.7-24-24V192h-87.7c-17.8 0-26.7-21.5-14.1-34.1L242.3 5.7c7.5-7.5 19.8-7.5 27.3 0l152.2 152.2c12.6 12.6 3.7 34.1-14.1 34.1H320v168c0 13.3-10.7 24-24 24zm216-8v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V376c0-13.3 10.7-24 24-24h136v8c0 30.9 25.1 56 56 56h80c30.9 0 56-25.1 56-56v-8h136c13.3 0 24 10.7 24 24zm-124 88c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20zm64 0c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20z"></path></svg> Can you snap a picture of the image your selling?</a>





<!--                                        <input type="file" name="pic" accept="image/*">-->
                                        <div class="form-section-sell-section__input-wrap form-section-sell-section__input-wrap_margin-top"><input class="button button_primary" type="button" value="Submit" /></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'our_process_buying' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="our-process-secondary">
                <div class="container">
                    <div class="our-process-secondary__title"><?php the_sub_field('title'); ?></div>
                    <div class="list-our-process-secondary">
                        <div class="list-our-process-secondary__item">
                            <div class="list-our-process-secondary__chart">01</div>
                            <div class="list-our-process-secondary__content">
                                <p><?php the_sub_field('text_1'); ?></p>
                            </div>
                        </div>
                        <div class="list-our-process-secondary__item">
                            <div class="list-our-process-secondary__chart">02</div>
                            <div class="list-our-process-secondary__content">
                                <p><?php the_sub_field('text_2'); ?></p>
                            </div>
                        </div>
                        <div class="list-our-process-secondary__item">
                            <div class="list-our-process-secondary__chart">03</div>
                            <div class="list-our-process-secondary__content">
                                <p><?php the_sub_field('text_3'); ?></p>
                            </div>
                        </div>
                        <div class="list-our-process-secondary__item">
                            <div class="list-our-process-secondary__chart">04</div>
                            <div class="list-our-process-secondary__content">
                                <p><?php the_sub_field('text_4'); ?></p>
                            </div>
                        </div>
                        <div class="list-our-process-secondary__item">
                            <div class="list-our-process-secondary__chart">05</div>
                            <div class="list-our-process-secondary__content">
                                <p><?php the_sub_field('text_5'); ?></p>
                            </div>
                        </div>
                        <div class="list-our-process-secondary__item">
                            <div class="list-our-process-secondary__chart">06</div>
                            <div class="list-our-process-secondary__content">
                                <p><?php the_sub_field('text_6'); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        <?php endif;

        elseif( get_row_layout() == 'what_we_buy' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>

            <section class="what-we-buy">
                <div class="container">
                    <div class="what-we-buy__title"><?php the_sub_field('title'); ?></div>
                    <div class="list-what-we-buy row">
                        <div class="col-xl-10 offset-xl-1 col-lg-10 offset-lg-1 col-md-12">
                            <div class="list-what-we-buy__item">
                                <div class="row align-items-center">
                                    <div class="col-xl-5 col-lg-5 col-md-4">
                                        <div class="list-what-we-buy__img"><img src="<?php the_sub_field('image_1'); ?>"></div>
                                    </div>
                                    <div class="col-xl-7 col-lg-7 col-md-8">
                                        <div class="list-what-we-buy__title"><?php the_sub_field('title_1'); ?></div>
                                        <div class="list-what-we-buy__sub-title"><?php the_sub_field('description_1'); ?></div>
                                        <div class="list-what-we-buy__text">
                                            <p><?php the_sub_field('text_1'); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="list-what-we-buy__item">
                                <div class="row align-items-center">
                                    <div class="col-xl-5 col-lg-5 col-md-4">
                                        <div class="list-what-we-buy__img"><img src="<?php the_sub_field('image_2'); ?>"></div>
                                    </div>
                                    <div class="col-xl-7 col-lg-7 col-md-8">
                                        <div class="list-what-we-buy__title"><?php the_sub_field('title_2'); ?></div>
                                        <div class="list-what-we-buy__sub-title"><?php the_sub_field('description_2'); ?></div>
                                        <div class="list-what-we-buy__text">
                                            <p><?php the_sub_field('text_2'); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="list-what-we-buy__item">
                                <div class="row align-items-center">
                                    <div class="col-xl-5 col-lg-5 col-md-4">
                                        <div class="list-what-we-buy__img"><img src="<?php the_sub_field('image_3'); ?>"></div>
                                    </div>
                                    <div class="col-xl-7 col-lg-7 col-md-8">
                                        <div class="list-what-we-buy__title"><?php the_sub_field('title_3'); ?></div>
                                        <div class="list-what-we-buy__sub-title"><?php the_sub_field('description_2'); ?></div>
                                        <div class="list-what-we-buy__text">
                                            <p><?php the_sub_field('text_3'); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        <?php endif;

        elseif( get_row_layout() == 'type_of_appraisals' ):
		  $show = get_sub_field('show_hide');
		  if ($show == "1"): ?>
              <section class="type-of-appraisals">
                  <div class="container">
                      <div class="type-of-appraisals__title"><?php the_sub_field('title'); ?></div>
                      <div class="list-type-of-appraisals row">
                          <div class="col-xl-6">
                              <div class="list-type-of-appraisals__item">
                                  <div class="list-type-of-appraisals__top">
                                      <div class="list-type-of-appraisals__svg"><svg viewbox="48.524 129.8 671.129 765.601"><path d="M648.9,297.05c-0.9-5.55-5.551-9.45-11.101-9.45c-105.899,0-246.6-83.7-247.95-84.6    c-3.6-2.1-8.1-2.1-11.55,0c-1.35,0.9-141.9,84.6-247.95,84.6c-5.55,0-10.2,4.05-11.1,9.45c-0.15,1.35-5.4,34.8-5.25,85.2    c0,6.15,5.1,11.25,11.25,11.25l0,0c6.3,0,11.25-5.1,11.25-11.25c0-33.3,2.4-59.25,3.9-72.3c45.45-1.95,100.2-16.65,162.9-43.8    c38.7-16.8,68.851-33.45,81-40.35c12.15,6.9,42.3,23.55,81,40.35C528,293.3,582.75,308,628.2,309.95    c3.149,27.9,10.35,115.05-10.65,209.1c-1.35,6.15,2.4,12.15,8.55,13.5c0.75,0.15,1.65,0.3,2.4,0.3c5.1,0,9.75-3.6,10.95-8.85    C665.55,405.95,649.65,301.55,648.9,297.05z"></path><path d="M621.9,547.55c-6-1.8-12.301,1.65-14.101,7.5c-18.3,61.05-46.95,112.65-85.2,153.15    c-37.05,39.45-83.699,69-138.75,87.899c-85.2-29.25-149.7-83.699-191.55-162c-31.35-58.5-49.8-129-54.75-209.55    c-0.45-6.149-5.7-10.95-12-10.5c-6.15,0.45-10.95,5.7-10.5,12c5.25,83.7,24.6,157.351,57.45,218.7    c45.3,84.6,115.2,143.25,207.9,174c1.199,0.45,2.399,0.6,3.6,0.6s2.4-0.149,3.6-0.6c60.15-19.95,111.15-51.9,151.5-95.1    c40.5-43.2,70.95-97.65,90.301-162.15C631.2,555.65,627.9,549.35,621.9,547.55z"></path><path d="M712.95,245.45c-0.9-5.55-5.55-9.45-11.101-9.45c-133.5,0-310.199-105.15-312-106.2    c-3.6-2.1-8.1-2.1-11.55,0c-1.8,1.05-178.5,106.2-312,106.2c-5.55,0-10.2,4.05-11.1,9.45c-0.75,5.25-19.8,130.05,10.05,273.3    C82.95,603.35,114,676.4,157.8,736.1C212.7,810.95,287.7,864.65,380.55,895.4c1.2,0.449,2.4,0.6,3.601,0.6    c1.199,0,2.399-0.15,3.6-0.6C480.6,864.5,555.6,810.95,610.5,736.1c43.8-59.699,75-132.75,92.55-217.35    C732.75,375.5,713.7,250.7,712.95,245.45z M680.85,514.1C663.9,595.4,634.05,665.6,592.2,722.75    C540.75,792.8,470.7,843.35,384,872.9c-86.55-29.551-156.6-79.95-207.9-149.851c-41.85-57-71.7-127.05-88.8-208.2    C62.7,397.4,72.3,290,76.05,258.35c57-1.95,126-20.25,205.35-54.75c49.95-21.6,88.5-43.05,102.6-51.15    c14.1,8.1,52.65,29.55,102.6,51.15c79.351,34.35,148.351,52.8,205.351,54.75C695.7,289.85,705.3,396.8,680.85,514.1z"></path><path d="M393.75,453.35l58.5-54.3l0.15-0.149c0,0,0,0,0.149-0.15c0.15-0.15,0.3-0.3,0.3-0.45l0,0    c0.15-0.149,0.15-0.3,0.301-0.45l0,0l0,0c0.149-0.149,0.149-0.3,0.3-0.6c0,0,0,0,0-0.15c0-0.149,0.149-0.3,0.149-0.449    c0,0,0,0,0-0.15s0-0.3,0.15-0.6v-0.15c0-0.15,0-0.3,0-0.45l0,0c0-0.15,0-0.3,0-0.6v-0.15c0-0.3-0.15-0.75-0.3-1.05v-0.15    c-0.15-0.15-0.15-0.3-0.3-0.45l-25.65-37.65c0,0,0-0.15-0.15-0.15c0-0.15-0.149-0.15-0.149-0.3l-0.15-0.15    c-0.149-0.15-0.149-0.15-0.3-0.3l-0.15-0.15c-0.149-0.15-0.149-0.15-0.3-0.3c0,0-0.149,0-0.149-0.15    c-0.15-0.15-0.301-0.15-0.45-0.3l0,0c-0.601-0.3-1.351-0.6-2.101-0.6h-82.05c-0.149,0-0.45,0-0.6,0c-1.351,0.15-2.55,0.9-3.3,2.1    l-25.351,37.8c-0.149,0.15-0.149,0.3-0.3,0.45v0.15c-0.15,0.3-0.3,0.75-0.3,1.05v0.15c0,0.15,0,0.3,0,0.6l0,0c0,0.15,0,0.3,0,0.45    v0.15c0,0.15,0,0.3,0.149,0.6c0,0,0,0,0,0.15s0.15,0.3,0.15,0.45c0,0,0,0,0,0.149c0,0.15,0.15,0.301,0.3,0.601l0,0l0,0    c0.15,0.149,0.15,0.3,0.3,0.45l0,0c0.15,0.149,0.301,0.3,0.301,0.449c0,0,0,0,0.149,0.15l0.15,0.15l58.5,54.3    c-47.101,5.55-83.7,45.75-83.7,94.2c0,52.35,42.6,94.949,94.95,94.949c52.35,0,94.95-42.6,94.95-94.949    C477.45,499.1,440.7,458.9,393.75,453.35z M392.85,441.35L406.5,400.4h30.45L392.85,441.35z M382.5,442.4l-13.95-42.15h28.05    L382.5,442.4z M424.05,366.95l16.05,24H411L424.05,366.95z M415.65,362.6l-12.601,23.25l-12.6-23.25H415.65z M395.1,390.95H369.9    l12.6-23.25L395.1,390.95z M374.55,362.6l-12.6,23.25L349.35,362.6C349.5,362.6,374.55,362.6,374.55,362.6z M341.1,366.8    l13.051,24H324.9L341.1,366.8z M328.05,400.25h30.601l13.649,41.1L328.05,400.25z M382.5,633.05c-47.1,0-85.5-38.399-85.5-85.5    c0-47.1,38.4-85.5,85.5-85.5s85.5,38.4,85.5,85.5C468,594.65,429.6,633.05,382.5,633.05z"></path><path d="M429.75,436.85c-2.4-1.05-5.1,0.15-6.15,2.551c-1.05,2.399,0.15,5.1,2.551,6.149    c40.949,17.55,67.35,57.601,67.35,102c0,61.2-49.8,110.851-110.85,110.851C321.6,658.4,271.8,608.6,271.8,547.55    c0-44.55,26.55-84.6,67.5-102.149c2.4-1.051,3.45-3.75,2.55-6.15c-1.05-2.4-3.75-3.45-6.149-2.55    c-44.55,18.899-73.35,62.399-73.35,110.7c0,66.3,54,120.3,120.3,120.3c66.3,0,120.3-54,120.3-120.3    C502.8,499.25,474.15,455.9,429.75,436.85z"></path></svg></div>
                                      <div class="list-type-of-appraisals__block">
                                          <div class="list-type-of-appraisals__title"><?php the_sub_field('title_1'); ?></div>
                                          <div class="list-type-of-appraisals__text">$<?php the_sub_field('price_2'); ?></div>
                                      </div>
                                  </div>
                                  <p><?php the_sub_field('text_1'); ?></p>
                              </div>
                          </div>
                          <div class="col-xl-6">
                              <div class="list-type-of-appraisals__item">
                                  <div class="list-type-of-appraisals__top">
                                      <div class="list-type-of-appraisals__svg"><svg viewbox="0 128 768 768"><path d="M666.56,597.92c-16.64-6.08-34.399-9.12-52.159-9.12c-17.44,0-34.721,3.04-51.2,8.96V217.6    c0-7.04-5.76-12.8-12.8-12.8H383.04c-6.4-44-44.16-76.8-88.64-76.8h-25.6c-44.48,0-82.24,32.8-88.64,76.8H12.8    c-7.04,0-12.8,5.76-12.8,12.8v665.6c0,7.04,5.76,12.8,12.8,12.8H550.4c5.6,0,10.56-3.84,12.159-9.28    c79.841,28.8,167.681-12.64,196.48-92.479C787.68,714.56,746.24,626.56,666.56,597.92z M448,230.4h89.6v379.2    c-9.119,5.28-17.76,11.521-25.6,18.721V268.8c0-7.04-5.76-12.8-12.8-12.8H448V230.4z M140.8,230.4H192c7.04,0,12.8-5.76,12.8-12.8    c0-35.36,28.64-64,64-64h25.6c35.36,0,64,28.64,64,64c0,7.04,5.76,12.8,12.8,12.8h51.2v51.2H140.8V230.4z M128,307.2h307.2    c7.04,0,12.8-5.76,12.8-12.8v-12.8h38.4v376c-32.32,48.48-34.24,111.2-4.801,161.601H76.8V281.6h38.4v12.8    C115.2,301.44,120.96,307.2,128,307.2z M25.6,870.4v-640h89.6V256H64c-7.04,0-12.8,5.76-12.8,12.8V832c0,7.04,5.76,12.8,12.8,12.8    h435.2c0.319,0,0.64-0.16,0.8-0.16c8.8,9.761,18.72,18.4,29.6,25.761H25.6z M614.4,870.4c-70.721,0-128-57.28-128-128    c0-70.721,57.279-128,128-128c70.72,0,128,57.279,128,128C742.24,813.12,685.12,870.4,614.4,870.4z"></path><path d="M623.68,704.64L664,667.2c0,0,0,0,0.16-0.16l0,0c0.16-0.16,0.16-0.16,0.32-0.32l0,0    c0-0.16,0.159-0.16,0.159-0.319l0,0l0,0c0-0.16,0.16-0.16,0.16-0.32l0,0c0-0.16,0-0.16,0.16-0.32l0,0c0-0.16,0-0.32,0-0.32    s0,0,0-0.159c0-0.16,0-0.16,0-0.32l0,0c0-0.16,0-0.32,0-0.32l0,0c0-0.319-0.16-0.479-0.32-0.8c0,0,0,0,0-0.16    s-0.159-0.16-0.159-0.32l-17.44-26.56l0,0l-0.16-0.16c0,0,0,0-0.16-0.159l-0.16-0.16c0,0,0,0-0.159,0l-0.16-0.16l0,0    c-0.16,0-0.16-0.16-0.32-0.16l0,0c-0.48-0.16-0.96-0.32-1.44-0.32h-56.64c-0.16,0-0.32,0-0.479,0    c-0.96,0.16-1.761,0.641-2.24,1.44l-17.44,26.08c0,0.16-0.16,0.16-0.16,0.319c0,0,0,0,0,0.16c-0.159,0.32-0.159,0.48-0.319,0.8    l0,0c0,0.16,0,0.32,0,0.32l0,0c0,0.16,0,0.32,0,0.32s0,0,0,0.16s0,0.319,0,0.319l0,0c0,0.16,0,0.16,0.16,0.32l0,0    c0,0.16,0.159,0.32,0.159,0.32l0,0l0,0c0,0.16,0.16,0.16,0.16,0.32l0,0c0,0.159,0.16,0.159,0.32,0.319l0,0c0,0,0,0,0.16,0.16    l40.32,37.44C576,708.32,550.72,736,550.72,769.44c0,36.159,29.44,65.439,65.44,65.439S681.6,805.44,681.6,769.44    C681.44,736.16,656.16,708.48,623.68,704.64z M623.04,696.32l9.44-28.32h20.96L623.04,696.32z M616,696.96L606.4,668h19.359    L616,696.96z M644.64,644.96l11.04,16.48h-20.16L644.64,644.96z M638.88,641.92l-8.64,16l-8.641-16    C621.44,641.92,638.88,641.92,638.88,641.92z M624.64,661.44H607.2l8.64-16L624.64,661.44z M610.56,641.92l-8.64,16l-8.64-16    C593.12,641.92,610.56,641.92,610.56,641.92z M587.36,644.96l8.96,16.64h-20.16L587.36,644.96z M578.4,668h21.119l9.44,28.32    L578.4,668z M616,828.48c-32.48,0-58.88-26.4-58.88-58.881c0-32.479,26.399-58.88,58.88-58.88s58.88,26.4,58.88,58.88    C674.88,802.08,648.48,828.48,616,828.48z"></path><path d="M648.64,693.28c-1.6-0.641-3.52,0-4.319,1.76c-0.641,1.6,0,3.52,1.76,4.32    c28.16,12,46.4,39.68,46.4,70.399c0,42.24-34.24,76.48-76.48,76.48s-76.48-34.24-76.48-76.48c0-30.72,18.24-58.399,46.561-70.399    c1.6-0.641,2.4-2.561,1.76-4.32c-0.64-1.6-2.56-2.4-4.32-1.76c-30.72,13.12-50.56,43.04-50.56,76.479    c0,45.76,37.28,83.04,83.04,83.04s83.04-37.28,83.04-83.04C699.04,736.32,679.2,706.24,648.64,693.28z"></path><rect x="137.6" y="400" width="185.6" height="20.8"></rect><rect x="137.6" y="464" width="185.6" height="20.8"></rect><rect x="137.6" y="528" width="276" height="20.8"></rect><rect x="137.6" y="592" width="276" height="20.8"></rect><rect x="137.6" y="656" width="276" height="20.8"></rect><rect x="311.2" y="752" width="102.399" height="20.8"></rect></svg></div>
                                      <div class="list-type-of-appraisals__block">
                                          <div class="list-type-of-appraisals__title"><?php the_sub_field('title_2'); ?></div>
                                          <div class="list-type-of-appraisals__text">$<?php the_sub_field('price_2'); ?></div>
                                      </div>
                                  </div>
                                  <p><?php the_sub_field('text_2'); ?></p>
                              </div>
                          </div>
                      </div>
                  </div>
              </section>
          <?php endif;

        elseif( get_row_layout() == 'appraisal_our_process' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="our-process">
                <div class="container">
                    <div class="our-process__title"><?php the_sub_field('title'); ?></div>
                    <div class="list-our-process">
                        <div class="row">
                            <div class="list-our-process__item">
                                <div class="list-our-process__chart">01</div>
                                <div class="list-our-process__content">
                                    <div class="title"><?php the_sub_field('title_1'); ?></div>
                                    <p><?php the_sub_field('text_1'); ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="list-our-process__item">
                                <div class="list-our-process__chart">02</div>
                                <div class="list-our-process__content">
                                    <div class="title"><?php the_sub_field('title_2'); ?></div>
                                    <p><?php the_sub_field('text_2'); ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="list-our-process__item">
                                <div class="list-our-process__chart">03</div>
                                <div class="list-our-process__content">
                                    <div class="title"><?php the_sub_field('title_3'); ?></div>
                                    <p><?php the_sub_field('text_3'); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'watch_our_brands' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="our-partners-page">
                <div class="container">
                    <div class="our-partners-page__title"><?php the_sub_field('title'); ?></div>
                    <div class="our-partners-page__sub-title"><?php the_sub_field('description'); ?></div>
                    <div class="list-our-partners">

					  <?php
					  if( have_rows('our_partners_partners') ):
						while ( have_rows('our_partners_partners') ) : the_row(); ?>

                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                <a class="list-our-partners__item" href="<?php the_sub_field('url'); ?>">
                                    <img class="list-our-partners__img" src="<?php the_sub_field('image'); ?>" alt="" role="presentation"/>
                                </a>
                            </div>

						<?php  endwhile;
					  else :
					  endif;
					  ?>

                    </div>
                    <div class="nav-arrows-slider">
                        <div class="nav-arrows-slider__left"><svg viewbox="90.838 84.5 430.324 617.5"><path d="M97.31,289.887c-8.629,8.337-8.629,22.214,0,30.843c8.338,8.337,22.214,8.337,30.532,0l156.391-156.392  v516.07c0.02,12.03,9.582,21.592,21.612,21.592s21.902-9.562,21.902-21.592v-516.07l156.101,156.392  c8.629,8.337,22.524,8.337,30.843,0c8.629-8.629,8.629-22.525,0-30.843L321.275,96.472c-8.337-8.629-22.213-8.629-30.532,0  L97.31,289.887z"></path></svg></div>
                        <div class="nav-arrows-slider__right"><svg viewbox="90.838 84.5 430.324 617.5"><path d="M97.31,289.887c-8.629,8.337-8.629,22.214,0,30.843c8.338,8.337,22.214,8.337,30.532,0l156.391-156.392  v516.07c0.02,12.03,9.582,21.592,21.612,21.592s21.902-9.562,21.902-21.592v-516.07l156.101,156.392  c8.629,8.337,22.524,8.337,30.843,0c8.629-8.629,8.629-22.525,0-30.843L321.275,96.472c-8.337-8.629-22.213-8.629-30.532,0  L97.31,289.887z"></path></svg></div>
                    </div>
                </div>
            </section>

		<?php endif;

        elseif( get_row_layout() == 'watch_services' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="robsons-service">
                <div class="container">
                    <div class="robsons-service__title"><?php the_sub_field('title'); ?></div>
                    <div class="list-robsons-service row">
                        <div class="col-xl-4">
                            <div class="list-robsons-service__item">
                                <div class="list-robsons-service__top"><img class="list-robsons-service__icon" src="<?php the_sub_field('image_1'); ?>" alt="" role="presentation" />
                                    <div class="list-robsons-service__chart">01</div>
                                </div>
                                <div class="list-robsons-service__title"><?php the_sub_field('title_1'); ?></div>
                                <div class="list-robsons-service__text"><?php the_sub_field('text_1'); ?></div>
                            </div>
                        </div>
                        <div class="col-xl-4">
                            <div class="list-robsons-service__item">
                                <div class="list-robsons-service__top"><img class="list-robsons-service__icon" src="<?php the_sub_field('image_2'); ?>" alt="" role="presentation" />
                                    <div class="list-robsons-service__chart">02</div>
                                </div>
                                <div class="list-robsons-service__title"><?php the_sub_field('title_2'); ?></div>
                                <div class="list-robsons-service__text"><?php the_sub_field('text_2'); ?></div>
                            </div>
                        </div>
                        <div class="col-xl-4">
                            <div class="list-robsons-service__item">
                                <div class="list-robsons-service__top"><img class="list-robsons-service__icon" src="<?php the_sub_field('image_3'); ?>" alt="" role="presentation" />
                                    <div class="list-robsons-service__chart">03</div>
                                </div>
                                <div class="list-robsons-service__title"><?php the_sub_field('title_3'); ?></div>
                                <div class="list-robsons-service__text"><?php the_sub_field('text_3'); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'get_a_quote_for_your_custom_designed_ring' ):
		  $show = get_sub_field('show_hide');
		  if ($show == "1"):?>
              <section class="block-custom-design">
                  <div class="container">
                      <div class="row">
                          <div class="col-xl-10 offset-xl-1 col-lg-10 offset-lg-1">
                              <div class="form-custom-design">
                                  <div class="form-custom-design__wrapper">
                                      <div class="form-custom-design__title">Get a Quote for Your Custom Designed Ring</div>
                                      <div class="form-custom-design__sub-title">Send us some information about your design and we'll give you a quote in 3hrs!</div>
                                      <form class="form-custom-design-section">
                                          <div class="form-custom-design-section__input-wrap"><input class="form-custom-design-section__input" placeholder="Your name" type="text" />
                                              <div class="form-custom-design-section__icon"><svg viewbox="83.606 121.611 599.97 774.54"><path d="M384,503.6c85.35,0,154.65-84.149,154.65-187.8C538.65,212.15,515.85,128,384,128   c-131.85,0-154.65,84.15-154.65,187.8C229.35,419.45,298.65,503.6,384,503.6z"></path><path d="M91.95,790.4C91.95,784.1,91.95,788.6,91.95,790.4L91.95,790.4z"></path><path d="M676.05,795.35C676.05,793.55,676.05,783.35,676.05,795.35L676.05,795.35z"></path><path d="M675.75,782.75c-2.85-180.75-26.4-232.2-207-264.75c0,0-25.35,32.4-84.6,32.4S299.4,518,299.4,518   c-178.65,32.25-203.7,82.95-207,258.9c-0.3,14.399-0.45,15.149-0.45,13.5c0,3.149,0,8.85,0,19.05c0,0,43.05,86.7,292.05,86.7   s292.05-86.7,292.05-86.7c0-6.45,0-10.95,0-14.101C676.05,796.25,675.9,794.3,675.75,782.75z"></path></svg></div>
                                          </div>
                                          <div class="form-custom-design-section__input-wrap"><input class="form-custom-design-section__input" placeholder="Your phone" type="text" />
                                              <div class="form-custom-design-section__icon"><svg viewbox="0 0 512 512"><path d="M493.4 24.6l-104-24c-11.3-2.6-22.9 3.3-27.5 13.9l-48 112c-4.2 9.8-1.4 21.3 6.9 28l60.6 49.6c-36 76.7-98.9 140.5-177.2 177.2l-49.6-60.6c-6.8-8.3-18.2-11.1-28-6.9l-112 48C3.9 366.5-2 378.1.6 389.4l24 104C27.1 504.2 36.7 512 48 512c256.1 0 464-207.5 464-464 0-11.2-7.7-20.9-18.6-23.4z"></path></svg></div>
                                          </div>
                                          <div class="form-custom-design-section__input-wrap"><input class="form-custom-design-section__input" placeholder="Your email" type="text" />
                                              <div class="form-custom-design-section__icon"><svg viewbox="0 177.497 612 437.006"><path d="M598.135,189.331c-7.89-7.292-18.408-11.833-30.242-11.833H43.987c-11.714,0-22.232,4.542-30.122,11.953    L306,439.748L598.135,189.331z"></path><path d="M306,483.377l-75.663-66.22L13.866,602.67c7.889,7.291,18.527,11.833,30.241,11.833h523.906    c11.714,0,22.232-4.542,30.122-11.833L381.783,417.157L306,483.377z"></path><polygon points="0,215.866 0,579.002 211.212,399.467"></polygon><polygon points="400.788,399.467 612,579.002 612,215.627"></polygon></svg></div>
                                          </div>
                                          <div class="form-custom-design-section__wrap-checkbox">
                                              <div class="wrapper one">
                                                  <div class="form-custom-design-section__wrap-input"><span>Tell us about yourself or the person your making this for.</span>
                                                      <div class="form-custom-design-section__input-wrap"><textarea class="form-custom-design-section__input form-custom-design-section__input_textarea"></textarea></div>
                                                  </div>
                                                  <div class="form-custom-design-section__wrap-input"><span>Who is this piece being designed for?</span>
                                                      <div class="form-custom-design-section__input-wrap"><textarea class="form-custom-design-section__input form-custom-design-section__input_textarea"></textarea></div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="form-custom-design-section__wrap-checkbox"><span>Preferred Metal Type</span>
                                              <div class="wrapper"><label class="checkbox"><input type="radio" name="order" value="0"><div class="icon"></div>14K Yellow Gold</label><label class="checkbox"><input type="radio" name="order" value="0" checked><div class="icon"></div>18K Yellow Gold</label>
                                                  <label
                                                          class="checkbox"><input type="radio" name="order" value="0">
                                                      <div class="icon"></div>14K Rose Gold</label><label class="checkbox"><input type="radio" name="order" value="0"><div class="icon"></div>18K Rose Gold</label><label class="checkbox"><input type="radio" name="order" value="0"><div class="icon"></div>14K White Gold</label>
                                                  <label
                                                          class="checkbox"><input type="radio" name="order" value="0">
                                                      <div class="icon"></div>18K White Gold</label><label class="checkbox"><input type="radio" name="order" value="0"><div class="icon"></div>1Platinum</label></div>
                                          </div>
                                          <div class="form-custom-design-section__wrap-checkbox">
                                              <div class="wrapper one">
                                                  <div class="form-custom-design-section__wrap-input"><span>Tell us about yourself or the person your making this for.</span>
                                                      <div class="form-custom-design-section__input-wrap"><textarea class="form-custom-design-section__input form-custom-design-section__input_textarea"></textarea></div>
                                                  </div>
                                                  <div class="form-custom-design-section__wrap-input"><span>Center stone</span>
                                                      <div class="form-custom-design-section__input-wrap"><select class="select-box"><option>I have my own center stone</option><option>I have my own center stone</option><option>I have my own center stone</option><option>I have my own center stone</option></select></div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="form-custom-design-section__wrap-checkbox"><span>Describe the type of jewelry this person wears, owns, aspires to own, or loves. Please include any specific links if available.<div class="form-custom-design-section__input-wrap"><textarea class="form-custom-design-section__input form-custom-design-section__input_textarea"></textarea></div></span></div>
                                          <div
                                                  class="form-custom-design-section__wrap-checkbox">
                                              <div class="wrapper one">
                                                  <div class="form-custom-design-section__wrap-input form-custom-design-section__wrap-input_custom-one"><span>What is your budget</span>
                                                      <div class="form-custom-design-section__input-wrap"><input class="form-custom-design-section__input" type="text" /></div>
                                                  </div>
                                                  <div class="form-custom-design-section__wrap-input form-custom-design-section__wrap-input_custom-two"><span>Any other comments or words that might help in creating the piece?</span>
                                                      <div class="form-custom-design-section__input-wrap"><textarea class="form-custom-design-section__input form-custom-design-section__input_textarea"></textarea></div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="form-custom-design-section__wrap-checkbox">
                                              <div class="wrapper one">
                                                  <div class="form-custom-design-section__wrap-input"><span>Do you have gold or platinum jewelry that you would like to trade in?</span>
                                                      <div class="form-custom-design-section__input-wrap"><textarea class="form-custom-design-section__input form-custom-design-section__input_textarea"></textarea></div>
                                                  </div>
                                                  <div class="form-custom-design-section__wrap-input"><span>How did you hear abount Robson's Custom Designs?</span>
                                                      <div class="form-custom-design-section__input-wrap"><textarea class="form-custom-design-section__input form-custom-design-section__input_textarea"></textarea></div>
                                                  </div>
                                              </div>
                                          </div><a class="form-custom-design-section__download" href="#"><svg viewbox="0 0 512 512"><path d="M296 384h-80c-13.3 0-24-10.7-24-24V192h-87.7c-17.8 0-26.7-21.5-14.1-34.1L242.3 5.7c7.5-7.5 19.8-7.5 27.3 0l152.2 152.2c12.6 12.6 3.7 34.1-14.1 34.1H320v168c0 13.3-10.7 24-24 24zm216-8v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V376c0-13.3 10.7-24 24-24h136v8c0 30.9 25.1 56 56 56h80c30.9 0 56-25.1 56-56v-8h136c13.3 0 24 10.7 24 24zm-124 88c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20zm64 0c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20z">       </path></svg>Do you have any pictures?</a>
                                          <div
                                                  class="form-custom-design-section__input-wrap form-custom-design-section__input-wrap_margin-top"><input class="button button_primary" type="button" value="Submit" /></div>
                                      </form>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </section>
        <?php endif;

        elseif( get_row_layout() == 'types_of_shanks' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"):?>
            <section class="types-of-shanks">
                <div class="container">
                    <div class="types-of-shanks__title"><?php the_sub_field('title'); ?></div>
                    <div class="list-types-of-shanks row">

					  <?php
					  if( have_rows('types_of_shanks_repeater') ):
						while ( have_rows('types_of_shanks_repeater') ) : the_row(); ?>

                            <div class="col-xl-3 col-lg-3 col-md-6">
                                <div class="list-types-of-shanks__item">
                                    <div class="list-types-of-shanks__img"><img src="<?php the_sub_field('image'); ?>"></div>
                                    <div class="list-types-of-shanks__title"><?php the_sub_field('name'); ?></div>
                                </div>
                            </div>

						<?php  endwhile;
					  else :
					  endif;
					  ?>

                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'types_of_mountings' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"):?>
            <section class="types-of-mountings">
                <div class="container">
                    <div class="types-of-mountings__title"><?php the_sub_field('title'); ?></div>
                    <div class="list-types-of-mountings row">
                        <div class="col-xl-8">
                            <div class="row">

                              <?php
                              if( have_rows('types_of_mountings_repeater') ):
                                while ( have_rows('types_of_mountings_repeater') ) : the_row(); ?>

                                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="list-types-of-mountings__item">
                                            <div class="list-types-of-mountings__img"><img src="<?php the_sub_field('image'); ?>"></div>
                                            <div class="list-types-of-mountings__title"><?php the_sub_field('name'); ?></div>
                                        </div>
                                    </div>

                                <?php  endwhile;
                              else :
                              endif;
                              ?>

                            </div>
                        </div>
                        <div class="col-xl-4">
                            <div class="list-types-of-mountings__big-item"><img src="<?php the_sub_field('big_image'); ?>"></div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'types_of_heads' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"):?>
            <section class="types-of-heads">
                <div class="container">
                    <div class="types-of-heads__title"><?php the_sub_field('title'); ?></div>
                    <div class="list-types-of-heads row">

					  <?php
					  if( have_rows('types_of_heads') ):
						while ( have_rows('types_of_heads') ) : the_row(); ?>

                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <div class="list-types-of-heads__item">
                                    <div class="list-types-of-heads__img"><img src="<?php the_sub_field('image'); ?>"></div>
                                    <div class="list-types-of-heads__title"><?php the_sub_field('name'); ?></div>
                                </div>
                            </div>

						<?php  endwhile;
					  else :
					  endif;
					  ?>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'anatomy_of_a_ring' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"):?>
            <section class="anatomy-of-ring">
                <div class="container">
                    <div class="anatomy-of-ring__title"><?php the_sub_field('title'); ?></div>
                    <div class="row">
                        <div class="col-xl-8 offset-xl-2 col-lg-8 offset-lg-2">
                            <div class="anatomy-of-ring__img"><img src="<?php the_sub_field('image'); ?>"></div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'custom_services_our_process' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"):?>
            <section class="our-process-third">
                <div class="container">
                    <div class="our-process-third__title"><?php the_sub_field('title'); ?></div>
                    <div class="list-our-process">

                      <?php
                      if( have_rows('our_process_repeater') ):
                        while ( have_rows('our_process_repeater') ) : the_row(); ?>

                            <div class="row">
                                <div class="list-our-process__item">
                                    <div class="list-our-process__chart"><?php the_sub_field('chart'); ?></div>
                                    <div class="list-our-process__content">
                                        <div class="title"><span><?php the_sub_field('step'); ?></span><?php the_sub_field('title'); ?></div>
                                        <p><?php the_sub_field('text'); ?></p>
                                    </div>
                                </div>
                            </div>

                        <?php  endwhile;
                      else :
                      endif;
                      ?>

                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'custom_services_love_is_unique' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"):?>
            <section class="why-section">
                <div class="container">
                    <div class="why-section__title"><?php the_sub_field('title'); ?></span></div>
                    <div class="why-section__text">
                        <p><?php the_sub_field('text'); ?></span></p>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'breadcrumb-section-design' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"):?>
            <section class="breadcrumb-section-design">
                <div class="container">
                    <div class="wrapper-top">
                        <div class="breadcrumb-section-design__title"><?php the_sub_field('title'); ?></div>
                        <a class="button button_primary click-modal-video" href="#"><i class="fa fa-play" aria-hidden="true"></i><?php the_sub_field('button_text'); ?></a></div>
                    <div class="wrapper-bottom">
                        <ul class="breadcrumb">
                            <li class="breadcrumb__item"><a href="/">Home</a></li>
                            <li class="breadcrumb__item"><span><?php the_sub_field('title'); ?></span></li>
                        </ul>
                    </div>
                </div>
                <div class="modal-video">
                    <div class="modal-video__wrapper">
                        <div class="modal-video__close"></div>
                        <div class="modal-video__iframe">
                            <iframe width="100%" height="620" src="https://www.youtube.com/embed/<?php the_sub_field('video_yotobe_id'); ?>?rel=0&amp;showinfo=0?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
                        </div>
                    </div>
                    <div class="bg-overlay-video"></div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'blog' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"):?>
            <section class="blog-recent-posts-pages">
                <div class="container">
                    <style>
                        .pagination li, .pagination a {
                            font-size: 14px;
                            color: #232b34;
                            font-weight: 400;
                            font-family: "Poppins", sans-serif;
                            min-width: 20px;
                            height: 20px;
                            display: flex;
                            align-items: center;
                            justify-content: center;
                            position: relative;
                            margin: 0 5px;
                            transition: all ease-in-out 0.4s;
                            padding: 0 5px;
                        }

                        .pagination .current {
                            background-color: #dfbc82;
                            color: #ffffff;
                        }

                        .pagination .current:after {
                            content: '';
                            width: 100%;
                            height: 100%;
                            background-color: transparent;
                            border: 1px solid #dfbc82;
                            position: absolute;
                            top: 3px;
                            left: 3px;
                            transition: all ease-in-out 0.4s;
                            z-index: 0;
                            opacity: 1;
                        }

                        .pagination a:hover{
                            background-color: #dfbc82;
                            color: #ffffff;
                        }


                    </style>
                    <div class="blog-recent-posts-pages__title"><?php the_sub_field('title'); ?></div>
                    <div class="blog-recent-posts-pages__sub-title"><?php the_sub_field('description'); ?></div>
                    <div class="list-blog-recent-posts">

                        <!--BLOG WHILE--> <?php
					  $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

                        $args = array(
                          'posts_per_page' => 9,
                          'paged' => $paged
                        );

                        $custom_query = new WP_Query( $args );

                        while($custom_query->have_posts()) :
                          $custom_query->the_post();
                        ?>

                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                                <div class="list-blog-recent-posts__item">
                                    <div class="list-blog-recent-posts__icon-wrap"><img class="list-blog-recent-posts__icon" src="<?php the_post_thumbnail_url();?>" alt="" role="presentation" />
                                        <div class="list-blog-recent-posts__block"><?php the_time('M d, Y') ?> | <span><?php the_category(', ') ?></span></div>
                                    </div><a class="list-blog-recent-posts__title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    <div class="list-blog-recent-posts__text"><?php the_excerpt(); ?></div>
                                </div>
                            </div>

						<?php endwhile; ?>

                    </div>
                    <div class="blog-recent-posts-pages__wrapper">
					  <?php if (function_exists("pagination")) {
						pagination($custom_query->max_num_pages);
					  } ?>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'testimonials_page' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"):?>
            <section class="testimonials-page">
                <div class="container">
                    <div class="testimonials-page__title"><?php the_sub_field('title'); ?></div>
                    <div class="testimonials-page__sub-title"><?php the_sub_field('description'); ?></div>
                    <div class="list-slide-testimonials-pages">

                      <?php
                      if( have_rows('global_testimonials',option) ):
                        while ( have_rows('global_testimonials',option) ) : the_row(); ?>

                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                                <div class="list-slide-testimonials-pages__item">
                                    <div class="list-slide-testimonials-pages__icon" style="background-image: url(<?php the_sub_field('image',option); ?>); ?>"></div>
                                    <div class="list-slide-testimonials-pages__title"><?php the_sub_field('title',option); ?></div>
                                    <div class="list-slide-testimonials-pages__text"><?php the_sub_field('text',option); ?></div>
                                    <div class="list-slide-testimonials__stars">

                                        </i><i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                    </div>
                                    <div class="list-slide-testimonials-pages__name"><?php the_sub_field('name',option); ?></div>
                                    <svg width="22px" height="20px" viewbox="0 0 612 792"><path fill="#EDC77C" d="M580.563,125.859H389.791c-17.332,0-31.437,14.104-31.437,31.437v190.772                    c0,17.332,14.104,31.437,31.437,31.437h91.322c-1.195,49.964-12.79,89.888-34.783,120.009                    c-17.332,23.668-43.51,43.391-78.652,58.93c-16.137,7.052-23.069,26.177-15.539,42.074l22.592,47.693                    c7.291,15.3,25.341,21.994,40.88,15.3c41.597-17.93,76.619-40.641,105.307-68.252c34.903-33.708,58.81-71.719,71.719-114.152                    S612,380.819,612,307.308V157.296C612,139.964,597.896,125.859,580.563,125.859z"></path><path fill="#EDC77C" d="M66.579,663.392c40.999-17.93,75.902-40.641,104.709-68.133c35.262-33.708,59.288-71.6,72.197-113.675                    c12.909-42.074,19.364-100.167,19.364-174.157V157.296c0-17.332-14.104-31.437-31.437-31.437H40.641                    c-17.212,0-31.317,14.104-31.317,31.437v190.772c0,17.332,14.105,31.437,31.437,31.437h91.322                    c-1.195,49.964-12.79,89.888-34.784,120.009c-17.332,23.668-43.509,43.391-78.651,58.93C2.51,565.495-4.423,584.62,3.108,600.518                    l22.591,47.574C32.871,663.392,51.04,670.205,66.579,663.392z"></path></svg></div>
                            </div>

                        <?php  endwhile;
                      else :	endif;
                      ?>

                    </div>
        <!--            <div class="testimonials-page__wrapper">-->
        <!--                <ul class="pagination">-->
        <!--                    <li class="page-item"><a class="page-link" href="#" tabindex="-1"><i class="fa fa-caret-left" aria-hidden="true"></i></a></li>-->
        <!--                    <li class="page-item active"><a class="page-link" href="#">1</a></li>-->
        <!--                    <li class="page-item"><a class="page-link" href="#">2 </a></li>-->
        <!--                    <li class="page-item"><a class="page-link" href="#">3</a></li>-->
        <!--                    <li class="page-item"><a class="page-link" href="#">...</a></li>-->
        <!--                    <li class="page-item"><a class="page-link" href="#">7  </a></li>-->
        <!--                    <li class="page-item"><a class="page-link" href="#"><i class="fa fa-caret-right" aria-hidden="true"></i></a></li>-->
        <!--                </ul>-->
        <!--            </div>-->
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'gallery' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"):?>
            <section class="gallery">
                <div class="container">
                    <div class="gallery__title"><?php the_sub_field('title'); ?></div>
                    <div class="row">
                        <div class="col-xl-10 offset-xl-1 col-lg-12">
                            <div class="list-gallery row">

                              <?php
                              if( have_rows('gallery_list') ):
                                while ( have_rows('gallery_list') ) : the_row(); ?>

                                    <div class="col-xl-4 col-lg-4">
                                        <div class="list-gallery__item"><img src="<?php the_sub_field('image'); ?>"></div>
                                    </div>

                                <?php  endwhile;
                              else :
                              endif;
                              ?>

                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'education_certifications' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"):?>
            <section class="education-certifications">
                <div class="container">
                    <div class="education-certifications__title"><?php the_sub_field('title'); ?></div>
                    <div class="list-education row">

                      <?php
                      if( have_rows('education_certifications_repeater') ):
                        while ( have_rows('education_certifications_repeater') ) : the_row(); ?>

                            <div class="col-xl-3 col-lg-3 col-md-6">
                                <div class="list-education__item">
                                    <div class="list-education__title"><?php the_sub_field('text'); ?></div>
                                </div>
                            </div>

                        <?php  endwhile;
                      else :
                      endif;
                      ?>

                    </div>
                    <div class="list-certifications">
                      <?php the_field('diamond_broker_list-certifications'); ?>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'your_personal' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"):?>
            <section class="diamond-broker">
                <div class="container">
                    <div class="diamond-broker__title"><?php the_sub_field('title'); ?></div>
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4">
                            <div class="info-broker">
                                <div class="info-broker__img"><img src="<?php the_sub_field('images'); ?>"></div>
                                <div class="info-broker__text">
                                    <div class="info-broker__title"><?php the_sub_field('title'); ?></div>
                                  <?php the_sub_field('image_text'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-8 col-lg-8 col-md-8">
                            <div class="diamond-broker__title-list"><?php the_sub_field('title_1'); ?></div>
                            <div class="diamond-broker__sub-title-list"><?php the_sub_field('description_1'); ?></div>
                          <?php the_sub_field('text_1'); ?>
                            <div class="diamond-broker__title-list-small"><?php the_sub_field('title_2'); ?></div>
                            <div class="diamond-broker__sub-title-list-small"><?php the_sub_field('description_2'); ?></div>
                          <?php the_sub_field('text_2'); ?>
                        </div>
                    </div>
                    <div class="block-watch-video-content">
                        <div class="content">
                            <div class="button button_primary click-modal-video"><i class="fa fa-play" aria-hidden="true"></i> <?php the_sub_field('button_text'); ?></div>
                        </div>
                    </div>
                </div>
                <div class="modal-video">
                    <div class="modal-video__wrapper">
                        <div class="modal-video__close"></div>
                        <div class="modal-video__iframe">
                            <iframe width="100%" height="620" src="https://www.youtube.com/embed/<?php the_sub_field('video_yotobe_id'); ?>?rel=0&amp;showinfo=0?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
                        </div>
                    </div>
                    <div class="bg-overlay-video"></div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'robsons_master_jeweler' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"):?>
            <section class="award-broker">
                <div class="container">
                    <div class="award-broker__title"><?php the_sub_field('title'); ?></div>
                    <div class="row align-items-center">
                        <div class="col-xl-4 col-lg-4 col-md-4">
                            <div class="info-broker">
                                <div class="info-broker__img"><img src="<?php the_sub_field('image'); ?>"></div>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-8 col-md-8">
                            <div class="award-broker__title-list"><?php the_sub_field('designer_name'); ?></div>
                            <div class="award-broker__sub-title-list"><?php the_sub_field('designer_title'); ?></div>
                          <?php the_sub_field('designer_text'); ?>
                        </div>
                    </div>
                    <div class="award-broker__separator"><img src="<?php the_sub_field('designer_image_line'); ?>"></div>
                    <div class="award-broker__list-certifications row">
                        <div class="col-xl-4 col-lg-4 col-md-12">
                          <?php the_sub_field('designer_text_2'); ?>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-12">
                          <?php the_sub_field('designer_text_3'); ?>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-12">
                          <?php the_sub_field('designer_text_4'); ?>
                        </div>
                    </div>
                    <div class="block-watch-video-content-secondary">
                        <div class="content">
                            <div class="button button_primary click-modal-video"><i class="fa fa-play" aria-hidden="true"></i><?php the_sub_field('button_text'); ?></div>
                        </div>
                    </div>
                </div>
                <div class="modal-video">
                    <div class="modal-video__wrapper">
                        <div class="modal-video__close"></div>
                        <div class="modal-video__iframe">
                            <iframe width="100%" height="620" src="https://www.youtube.com/embed/<?php the_sub_field('video_yotobe_id'); ?>?rel=0&amp;showinfo=0?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
                        </div>
                    </div>
                    <div class="bg-overlay-video"></div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'contact_us_form' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>

            <!-- SELECT FORM START -->
          <?php $case = get_sub_field('сontact_select'); ?>

          <?php if ($case == "standart") { ?>

                <!-- contact Standart-->
                <section class="contact-us-page">
                    <div class="container">
                        <div class="contact-us-page__title"><?php the_field('standart_title',option); ?></div>
                        <div class="contact-us-page__sub-title"><?php the_field('standart_description',option); ?></div>
                        <form class="form-contact-page row">
                            <div class="col-xl-4">
                                <div class="form-contact-page__input-wrap">
                                    <input id="sgn_name" class="form-contact-page__input" placeholder="<?php the_field('standart_input_name',option); ?>" type="text" />
                                    <div class="form-contact-page__icon">
                                        <svg viewbox="83.606 121.611 599.97 774.54"><path d="M384,503.6c85.35,0,154.65-84.149,154.65-187.8C538.65,212.15,515.85,128,384,128   c-131.85,0-154.65,84.15-154.65,187.8C229.35,419.45,298.65,503.6,384,503.6z"></path><path d="M91.95,790.4C91.95,784.1,91.95,788.6,91.95,790.4L91.95,790.4z"></path><path d="M676.05,795.35C676.05,793.55,676.05,783.35,676.05,795.35L676.05,795.35z"></path><path d="M675.75,782.75c-2.85-180.75-26.4-232.2-207-264.75c0,0-25.35,32.4-84.6,32.4S299.4,518,299.4,518   c-178.65,32.25-203.7,82.95-207,258.9c-0.3,14.399-0.45,15.149-0.45,13.5c0,3.149,0,8.85,0,19.05c0,0,43.05,86.7,292.05,86.7   s292.05-86.7,292.05-86.7c0-6.45,0-10.95,0-14.101C676.05,796.25,675.9,794.3,675.75,782.75z"></path></svg></div>
                                </div>
                            </div>
                            <div class="col-xl-4">
                                <div class="form-contact-page__input-wrap">
                                    <input id="sgn_email" class="form-contact-page__input" placeholder="<?php the_field('standart_input_email',option); ?>" type="text" />
                                    <div class="form-contact-page__icon">
                                        <svg viewbox="0 177.497 612 437.006"><path d="M598.135,189.331c-7.89-7.292-18.408-11.833-30.242-11.833H43.987c-11.714,0-22.232,4.542-30.122,11.953    L306,439.748L598.135,189.331z"></path><path d="M306,483.377l-75.663-66.22L13.866,602.67c7.889,7.291,18.527,11.833,30.241,11.833h523.906    c11.714,0,22.232-4.542,30.122-11.833L381.783,417.157L306,483.377z"></path><polygon points="0,215.866 0,579.002 211.212,399.467"></polygon><polygon points="400.788,399.467 612,579.002 612,215.627"></polygon></svg></div>
                                </div>
                            </div>
                            <div class="col-xl-4">
                                <div class="form-contact-page__input-wrap">
                                    <input id="sgn_phone" class="form-contact-page__input" placeholder="<?php the_field('standart_input_phone',option); ?>" type="text" />
                                    <div class="form-contact-page__icon">
                                        <svg viewbox="0 0 512 512"><path d="M493.4 24.6l-104-24c-11.3-2.6-22.9 3.3-27.5 13.9l-48 112c-4.2 9.8-1.4 21.3 6.9 28l60.6 49.6c-36 76.7-98.9 140.5-177.2 177.2l-49.6-60.6c-6.8-8.3-18.2-11.1-28-6.9l-112 48C3.9 366.5-2 378.1.6 389.4l24 104C27.1 504.2 36.7 512 48 512c256.1 0 464-207.5 464-464 0-11.2-7.7-20.9-18.6-23.4z"></path></svg></div>
                                </div>
                            </div>
                            <div class="col-xl-12">
                                <div class="form-contact-page__input-wrap">
                                    <textarea id="sgn_message" class="form-contact-page__input form-contact-page__input_textarea" placeholder="<?php the_field('standart_input_message',option); ?>"></textarea>
                                </div>
                            </div>
                            <div class="col-xl-2 offset-xl-5 col-lg-2 offset-lg-5">
                                <div class="form-contact-page__input-wrap">
                                    <button id="sgn_stndrt" class="button button_primary" type="button"><?php the_field('standart_button_text',option); ?></button></div>
                            </div>
                        </form>
                    </div>
                </section>

          <?php } ?>

          <?php if ($case == "custom") { ?>

                <!-- contact Custom -->
                <section class="contact-us-page">
                    <div class="container">
                        <div class="contact-us-page__title"><?php the_field('custom_form_title',option); ?></div>
                        <div class="contact-us-page__sub-title"><?php the_field('custom_form_title',option); ?></div>
                        <form class="form-contact-page row">
                            <div class="col-xl-4">
                                <div class="form-contact-page__input-wrap">
                                    <input class="form-contact-page__input" placeholder="<?php the_field('custom_input_name',option); ?>" type="text" />
                                    <div class="form-contact-page__icon">
                                        <svg viewbox="83.606 121.611 599.97 774.54"><path d="M384,503.6c85.35,0,154.65-84.149,154.65-187.8C538.65,212.15,515.85,128,384,128   c-131.85,0-154.65,84.15-154.65,187.8C229.35,419.45,298.65,503.6,384,503.6z"></path><path d="M91.95,790.4C91.95,784.1,91.95,788.6,91.95,790.4L91.95,790.4z"></path><path d="M676.05,795.35C676.05,793.55,676.05,783.35,676.05,795.35L676.05,795.35z"></path><path d="M675.75,782.75c-2.85-180.75-26.4-232.2-207-264.75c0,0-25.35,32.4-84.6,32.4S299.4,518,299.4,518   c-178.65,32.25-203.7,82.95-207,258.9c-0.3,14.399-0.45,15.149-0.45,13.5c0,3.149,0,8.85,0,19.05c0,0,43.05,86.7,292.05,86.7   s292.05-86.7,292.05-86.7c0-6.45,0-10.95,0-14.101C676.05,796.25,675.9,794.3,675.75,782.75z"></path></svg></div>
                                </div>
                            </div>
                            <div class="col-xl-4">
                                <div class="form-contact-page__input-wrap">
                                    <input class="form-contact-page__input" placeholder="Yo<?php the_field('custom_input_email',option); ?>" type="text" />
                                    <div class="form-contact-page__icon">
                                        <svg viewbox="0 177.497 612 437.006"><path d="M598.135,189.331c-7.89-7.292-18.408-11.833-30.242-11.833H43.987c-11.714,0-22.232,4.542-30.122,11.953    L306,439.748L598.135,189.331z"></path><path d="M306,483.377l-75.663-66.22L13.866,602.67c7.889,7.291,18.527,11.833,30.241,11.833h523.906    c11.714,0,22.232-4.542,30.122-11.833L381.783,417.157L306,483.377z"></path><polygon points="0,215.866 0,579.002 211.212,399.467"></polygon><polygon points="400.788,399.467 612,579.002 612,215.627"></polygon></svg></div>
                                </div>
                            </div>
                            <div class="col-xl-4">
                                <div class="form-contact-page__input-wrap">
                                    <input class="form-contact-page__input" placeholder="Y<?php the_field('custom_input_phone',option); ?>" type="text" />
                                    <div class="form-contact-page__icon">
                                        <svg viewbox="0 0 512 512"><path d="M493.4 24.6l-104-24c-11.3-2.6-22.9 3.3-27.5 13.9l-48 112c-4.2 9.8-1.4 21.3 6.9 28l60.6 49.6c-36 76.7-98.9 140.5-177.2 177.2l-49.6-60.6c-6.8-8.3-18.2-11.1-28-6.9l-112 48C3.9 366.5-2 378.1.6 389.4l24 104C27.1 504.2 36.7 512 48 512c256.1 0 464-207.5 464-464 0-11.2-7.7-20.9-18.6-23.4z"></path></svg></div>
                                </div>
                            </div>
                            <div class="col-xl-12">
                                <div class="form-contact-page__input-wrap">
                                    <textarea class="form-contact-page__input form-contact-page__input_textarea" placeholder="<?php the_field('custom_input_message',option); ?>"></textarea>
                                </div>
                            </div>
                            <div class="col-xl-2 offset-xl-5 col-lg-2 offset-lg-5">
                                <div class="form-contact-page__input-wrap">
                                    <button class="button button_primary" type="button"><?php the_field('custom_button_text',option); ?></button></div>
                            </div>
                        </form>
                    </div>
                </section>

          <?php } ?>
            <!-- SELECT FORM END -->

        <?php endif;

        elseif( get_row_layout() == 'recent_posts' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="blog-recent-posts">
                <div class="container" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-once="true">
                    <div class="blog-recent-posts__title"><?php the_sub_field('title'); ?></div>
                    <div class="blog-recent-posts__sub-title"><?php the_sub_field('description'); ?></div>
                    <div class="list-blog-recent-posts">

                        <!--BLOG WHILE-->
                      <?php
                      $query = new WP_Query('posts_per_page=3');
                      if( $query->have_posts() ){
                        while( $query->have_posts() ){ $query->the_post();
                          ?>

                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                                <div class="list-blog-recent-posts__item">
                                    <div class="list-blog-recent-posts__icon-wrap"><img class="list-blog-recent-posts__icon" src="<?php the_post_thumbnail_url();?>" alt="" role="presentation" />
                                        <div class="list-blog-recent-posts__block"><?php the_time('M d, Y') ?> | <span><?php the_category(', ') ?></span></div>
                                    </div><a class="list-blog-recent-posts__title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    <div class="list-blog-recent-posts__text"><?php the_excerpt(); ?></div>
                                </div>
                            </div>

                        <?php } wp_reset_postdata(); } ?>

                    </div><a class="button button_light" href="<?php the_sub_field('button_url'); ?>"><?php the_sub_field('button_text'); ?></a></div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'relax_your_covered' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="relax-your-covered">
                <div class="container">
                    <div class="relax-your-covered__wrapper row" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-once="true">
                        <div class="col-xl-5 col-lg-5 col-md-12">
                            <div class="relax-your-covered__title"><?php the_sub_field('title'); ?></div>
                          <?php the_sub_field('description'); ?>
                        </div>
                        <div class="relax-your-covered__img-wrap col-xl-7 col-lg-7 col-md-12"><img class="relax-your-covered__img" src="<?php the_sub_field('image'); ?>" alt="" role="presentation" /></div>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'instagram' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="instagram" >
                <div class="instagram__wrapper"><a class="button button_light" href="#"><?php the_sub_field('title'); ?></a>

                  <?php
                  if( have_rows('robsonjewelers_rep') ):
                    while ( have_rows('robsonjewelers_rep') ) : the_row(); ?>

                        <div class="instagram__item col-xl-2 col-lg-2 col-md-6 col-sm-12" style="background-image: url(<?php the_sub_field('image'); ?>);"></div>

                    <?php  endwhile;
                  else :
                  endif;
                  ?>

                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'our_brands_home' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="our-partners">
                <div class="container" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-once="true">
                    <div class="our-partners__title"><?php the_sub_field('title'); ?></div>
                    <div class="our-partners__sub-title"><?php the_sub_field('description'); ?></div>
                    <div class="list-our-partners">

                      <?php
                      if( have_rows('our_partners_partners') ):
                        while ( have_rows('our_partners_partners') ) : the_row(); ?>

                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                <a class="list-our-partners__item" href="<?php the_sub_field('url'); ?>">
                                    <img class="list-our-partners__img" src="<?php the_sub_field('image'); ?>" alt="" role="presentation"/>
                                </a>
                            </div>

                        <?php  endwhile;
                      else :
                      endif;
                      ?>

                    </div>
                    <div class="nav-arrows-slider">
                        <div class="nav-arrows-slider__left"><svg viewbox="90.838 84.5 430.324 617.5"><path d="M97.31,289.887c-8.629,8.337-8.629,22.214,0,30.843c8.338,8.337,22.214,8.337,30.532,0l156.391-156.392  v516.07c0.02,12.03,9.582,21.592,21.612,21.592s21.902-9.562,21.902-21.592v-516.07l156.101,156.392  c8.629,8.337,22.524,8.337,30.843,0c8.629-8.629,8.629-22.525,0-30.843L321.275,96.472c-8.337-8.629-22.213-8.629-30.532,0  L97.31,289.887z"></path></svg></div>
                        <div class="nav-arrows-slider__right"><svg viewbox="90.838 84.5 430.324 617.5"><path d="M97.31,289.887c-8.629,8.337-8.629,22.214,0,30.843c8.338,8.337,22.214,8.337,30.532,0l156.391-156.392  v516.07c0.02,12.03,9.582,21.592,21.612,21.592s21.902-9.562,21.902-21.592v-516.07l156.101,156.392  c8.629,8.337,22.524,8.337,30.843,0c8.629-8.629,8.629-22.525,0-30.843L321.275,96.472c-8.337-8.629-22.213-8.629-30.532,0  L97.31,289.887z"></path></svg></div>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'abou_us_home' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="about-us">
                <div class="container" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-once="true">
                    <div class="about-us__title"><?php the_sub_field('title'); ?></div>
                    <div class="about-us__wrapper row align-items-center">
                        <div class="col-xl-6 col-lg-6 col-md-12">
                            <div class="about-us__title-text"><?php the_sub_field('title_text'); ?></div>
                          <?php the_sub_field('description'); ?>
                            <a class="button button_primary" href="<?php the_sub_field('button_url'); ?>"><?php the_sub_field('button_text'); ?></a></div>
                        <div class="col-xl-6 col-lg-6 col-md-12"><img class="about-us__img" src="<?php the_sub_field('image'); ?>" alt="" role="presentation" /></div>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'sign_up_for_savings' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="sign-up">
                <div class="container" >
                    <div class="row">
                        <div class="col-xl-10 offset-xl-1 col-lg-10 offset-lg-1">
                            <div class="form-sign-up" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-once="true">
                                <div class="form-sign-up__wrapper">
                                    <div class="form-sign-up__title"><?php the_sub_field('title'); ?></div>
                                    <div class="form-sign-up__sub-title"><?php the_sub_field('description'); ?></div>
                                    <form class="form-sign-up-main">
                                        <div class="form-sign-up-main__input-wrap">
                                            <input id="sgn_name" class="form-sign-up-main__input" placeholder="Your name" type="text" />
                                            <div class="form-sign-up-main__icon"><svg viewbox="83.606 121.611 599.97 774.54"><path d="M384,503.6c85.35,0,154.65-84.149,154.65-187.8C538.65,212.15,515.85,128,384,128   c-131.85,0-154.65,84.15-154.65,187.8C229.35,419.45,298.65,503.6,384,503.6z"></path><path d="M91.95,790.4C91.95,784.1,91.95,788.6,91.95,790.4L91.95,790.4z"></path><path d="M676.05,795.35C676.05,793.55,676.05,783.35,676.05,795.35L676.05,795.35z"></path><path d="M675.75,782.75c-2.85-180.75-26.4-232.2-207-264.75c0,0-25.35,32.4-84.6,32.4S299.4,518,299.4,518   c-178.65,32.25-203.7,82.95-207,258.9c-0.3,14.399-0.45,15.149-0.45,13.5c0,3.149,0,8.85,0,19.05c0,0,43.05,86.7,292.05,86.7   s292.05-86.7,292.05-86.7c0-6.45,0-10.95,0-14.101C676.05,796.25,675.9,794.3,675.75,782.75z"></path></svg></div>
                                        </div>
                                        <div class="form-sign-up-main__input-wrap">
                                            <input id="sgn_email" class="form-sign-up-main__input" placeholder="Your email" type="text" />
                                            <div class="form-sign-up-main__icon"><svg viewbox="0 177.497 612 437.006"><path d="M598.135,189.331c-7.89-7.292-18.408-11.833-30.242-11.833H43.987c-11.714,0-22.232,4.542-30.122,11.953    L306,439.748L598.135,189.331z"></path><path d="M306,483.377l-75.663-66.22L13.866,602.67c7.889,7.291,18.527,11.833,30.241,11.833h523.906    c11.714,0,22.232-4.542,30.122-11.833L381.783,417.157L306,483.377z"></path><polygon points="0,215.866 0,579.002 211.212,399.467"></polygon><polygon points="400.788,399.467 612,579.002 612,215.627"></polygon></svg></div>
                                        </div>
                                        <div class="form-sign-up-main__input-wrap">
                                            <input id="sgn_submit" class="button button_primary" type="button" value="Newsletter Signup" />
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'loose_diamonds' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="loose-diamonds">
                <div class="container" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-once="true">
                    <div class="loose-diamonds__title"><?php the_sub_field('title'); ?></div>
                    <div class="loose-diamonds__sub-title"><?php the_sub_field('text'); ?></div>
                    <div class="list-loose-diamonds">

					  <?php
					  if( have_rows('loose_diamonds_rep') ):
						while ( have_rows('loose_diamonds_rep') ) : the_row(); ?>

                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                <a class="list-loose-diamonds__item" href="#">
                                    <img class="list-loose-diamonds__svg" src="<?php the_sub_field('image'); ?>" alt="" role="presentation"/>
                                    <span class="list-loose-diamonds__title"><?php the_sub_field('title'); ?></span>
                                </a>
                            </div>

						<?php  endwhile;
					  else :
					  endif;
					  ?>


                    </div>
                    <a class="button button_primary" href="<?php the_sub_field('button_url'); ?>"><?php the_sub_field('button_text'); ?></a></div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'why_choose_us_home' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="why-choose-us">
                <div class="container" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-once="true">
                    <div class="why-choose-us__title"><?php the_sub_field('title'); ?></div>
                    <div class="why-choose-us__sub-title"><?php the_sub_field('description'); ?></div>
                    <div class="list-why-choose-us">
                        <div class="col-xl-4 col-lg-4 col-md-12">
                            <div class="list-why-choose-us__item">
                                <div class="list-why-choose-us__front">
                                    <div class="list-why-choose-us__svg"><svg viewbox="25.2 128 717.166 768"><path d="M739.5,358.25C739.95,359.3,740.1,359.75,739.5,358.25c-1.2-2.85-1.8-4.05-1.8-4.2     C737.85,354.35,738.9,356.9,739.5,358.25c-5.25-11.85-13.5-16.65-22.2-21.6C608.25,274.1,538.8,208.4,531.6,161     c-2.85-18.75-19.5-33-38.85-33H404.4c-6.45,0-11.7,5.25-11.7,11.7c0,6.45,5.25,11.7,11.7,11.7h88.35c7.8,0,14.55,5.55,15.6,12.9     c5.101,34.5,33.601,73.5,82.351,114.3c-6,10.8-12.45,21.6-19.2,32.1c-5.4-2.1-11.25-3.3-17.25-3.3     c-26.4,0-47.85,21.45-47.85,47.85c0,10.8,3.6,20.85,9.75,28.95c-5.7,6.45-11.551,12.899-17.551,19.2     c-7.199-4.2-15.449-6.601-24.3-6.601c-26.399,0-47.85,21.45-47.85,47.851c0,7.35,1.649,14.25,4.649,20.55     c-10.949,8.7-22.35,17.1-33.899,25.2c-4.05-0.9-8.25-1.351-12.45-1.351c-4.35,0-8.4,0.45-12.45,1.351     c-11.25-7.801-22.35-16.051-33-24.601c3.15-6.45,4.95-13.649,4.95-21.3c0-8.1-2.1-16.2-6-23.4c-3.15-5.699-10.35-7.649-16.05-4.5     c-5.7,3.15-7.65,10.351-4.5,16.051c1.95,3.6,3,7.649,3,11.85c0,13.5-10.95,24.45-24.45,24.45S271.8,458,271.8,444.5     s10.95-24.45,24.45-24.45c6.45,0,11.7-5.25,11.7-11.7c0-6.449-5.25-11.699-11.7-11.699c-9.15,0-17.85,2.55-25.2,7.199     c-6.15-6.449-12.15-13.05-18-19.649c6.15-8.1,9.75-18,9.75-28.95c0-26.4-21.45-47.85-47.85-47.85c-6.15,0-12,1.2-17.25,3.3     c-8.55-13.35-16.5-26.85-23.85-40.65c41.7-39.45,65.1-75,69.75-105.75c1.05-7.35,7.8-12.9,15.6-12.9h98.55     c6.45,0,11.7-5.25,11.7-11.7c0-6.45-5.55-11.7-12.15-11.7h-98.55c-19.35,0-36,14.25-38.85,33     c-6.6,44.1-68.25,107.85-169.05,174.75c-11.4,7.65-17.7,11.85-22.5,22.5c-2.1,4.95-3.15,10.2-3.15,15.6     C26.4,510.8,62.4,639.65,126.45,737C194.25,839.45,285.6,896,384,896c98.4,0,189.75-56.55,257.4-159.15     c26.85-40.8,48.899-87.3,65.55-138.149c1.95-6.15-1.351-12.75-7.5-14.851c-6.15-1.949-12.75,1.351-14.851,7.5     c-16.05,48.9-37.199,93.601-62.85,132.601C558.6,819.8,474.15,872.45,384,872.45c-90.15,0-174.6-52.8-237.75-148.5     C84.6,630.35,49.95,506,48.75,373.55c0-2.1,0.45-4.05,1.2-6c1.8-4.2,3.3-5.25,13.95-12.3c25.05-16.65,59.7-41.1,91.5-68.85     c7.05,12.9,14.7,25.65,22.65,38.1c-7.05,8.4-11.4,19.2-11.4,31.05c0,26.4,21.45,47.851,47.85,47.851c7.05,0,13.8-1.5,19.8-4.351     c6.45,7.351,13.2,14.7,19.95,21.9c-4.05,7.05-6.45,15.149-6.45,23.85c0,26.4,21.45,47.851,47.85,47.851     c10.35,0,19.8-3.301,27.6-8.851c7.65,6.15,15.6,12.15,23.55,18c-14.85,11.25-24.45,29.101-24.45,49.2     c0,19.95,9.601,37.8,24.301,49.05c-38.25,36.15-60.601,84-60,130.65c0,4.05,0.45,8.25,0.9,12.3c0.45,4.95,0.9,10.05,2.4,14.7     c11.25,44.1,49.95,75,94.05,75c44.7,0,83.55-31.351,94.35-76.2c1.351-5.25,1.801-10.65,2.25-16.05     c0.301-3.3,0.601-6.601,0.601-9.75c0.149-15.45-2.101-31.2-6.75-46.8c-1.95-6.15-8.4-9.75-14.7-7.801     c-6.15,1.95-9.75,8.4-7.8,14.7c4.05,13.351,6,26.7,5.85,39.45c-0.149,7.35-1.2,14.4-2.399,21.6     c-8.551,33.9-37.801,57.45-71.4,57.45c-33.45,0-62.85-23.7-71.4-57.6c-1.8-7.05-2.25-14.55-2.399-21.601     c-0.45-42.75,21.899-87,59.399-119.1c4.65,1.05,9.45,1.65,14.4,1.65s9.75-0.601,14.25-1.65c15.15,13.05,28.2,28.35,38.1,44.85     c3.301,5.551,10.5,7.351,16.051,4.051c5.55-3.301,7.35-10.5,4.05-16.051C447,627.95,435,613.25,421.2,600.2     c14.85-11.25,24.45-29.101,24.45-49.05c0-20.101-9.601-37.95-24.45-49.2c8.25-6,16.35-12.15,24.149-18.45     c7.95,5.85,17.7,9.3,28.351,9.3c26.399,0,47.85-21.45,47.85-47.85c0-9-2.55-17.55-6.899-24.75c6.6-6.9,13.05-13.95,19.199-21     c6,2.7,12.75,4.35,19.801,4.35c26.399,0,47.85-21.45,47.85-47.85c0-11.85-4.35-22.65-11.4-31.05     c6.45-10.05,12.75-20.4,18.601-30.75c27.149,20.85,59.399,42.15,96.45,63.3c7.949,4.5,10.35,6,12.3,10.65     c0.899,1.95,1.2,3.9,1.2,6c-0.601,59.7-7.801,117.9-21.601,173.1c-1.649,6.3,2.25,12.75,8.55,14.25c0.9,0.3,1.95,0.3,2.851,0.3     c5.25,0,10.05-3.6,11.399-8.85C734.1,495.65,741.6,435.5,742.2,374C742.8,368.45,741.75,363.2,739.5,358.25z M214.65,380     c-13.5,0-24.45-10.95-24.45-24.45s10.95-24.45,24.45-24.45s24.45,10.95,24.45,24.45S228.15,380,214.65,380z M422.55,551.15     c0,21-17.1,38.1-38.1,38.1s-38.101-17.1-38.101-38.1s17.101-38.101,38.101-38.101S422.55,530.15,422.55,551.15z M474.15,469.4     c-13.5,0-24.45-10.95-24.45-24.45s10.95-24.45,24.45-24.45s24.449,10.95,24.449,24.45C498.45,458.45,487.5,469.4,474.15,469.4z      M554.1,380c-13.5,0-24.449-10.95-24.449-24.45S540.6,331.1,554.1,331.1s24.45,10.95,24.45,24.45S567.6,380,554.1,380z"></path><path d="M737.7,353.9L737.7,353.9L737.7,353.9z"></path><path fill="#DFBC82" d="M384.45,671.6c-34.05,0-61.65,27.601-61.65,61.65s27.601,61.65,61.65,61.65S446.1,767.3,446.1,733.25    C446.1,699.35,418.35,671.6,384.45,671.6z M384.45,771.5c-21,0-38.101-17.1-38.101-38.1S363.45,695.3,384.45,695.3    s38.1,17.101,38.1,38.101S405.45,771.5,384.45,771.5z"></path></svg></div>
                                    <div class="list-why-choose-us__title"><?php the_sub_field('title_1'); ?></div>
                                </div><a class="list-why-choose-us__back" href="#"><span class="list-why-choose-us__title"><?php the_sub_field('title_1'); ?></span>
                                    <span class="list-why-choose-us__text"><?php the_sub_field('text_1'); ?></span></a></div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-12">
                            <div class="list-why-choose-us__item">
                                <div class="list-why-choose-us__front">
                                    <div class="list-why-choose-us__svg"><svg viewbox="55.8 128 656.403 768.15"><path d="M700.5,128c-6.45,0-11.7,5.25-11.7,11.7c0,70.95-25.5,139.65-72,193.8c-4.2,4.95-3.6,12.3,1.2,16.65    c2.25,1.95,4.95,2.85,7.65,2.85c3.3,0,6.6-1.35,9-4.05c50.1-58.35,77.55-132.6,77.55-209.1C712.35,133.25,707.1,128,700.5,128z"></path><path fill="#DFBC82" d="M611.85,807.05c-25.649-7.05-27.6-9-34.8-34.8c-2.399-8.85-10.5-15-19.649-15c-9.15,0-17.25,6.15-19.65,15    c-7.05,25.65-9,27.6-34.8,34.8c-8.851,2.4-14.851,10.5-14.851,19.65c0,9.149,6.15,17.25,15,19.649    c25.65,7.051,27.601,9,34.801,34.801c2.399,8.85,10.5,15,19.649,15c9.15,0,17.25-6.15,19.65-15c7.05-25.65,9-27.601,34.8-34.801    c8.85-2.399,14.85-10.5,14.85-19.649C626.7,817.55,620.7,809.6,611.85,807.05z M557.55,864.65    c-7.35-22.2-15.75-30.601-37.95-37.95c22.2-7.351,30.601-15.75,37.95-37.95c7.351,22.2,15.75,30.6,37.95,37.95    C573.15,834.05,564.75,842.45,557.55,864.65z"></path><path fill="#DFBC82" d="M186.15,625.55c-22.95-6.3-24.6-8.1-31.05-31.05c-2.4-8.4-10.05-14.4-18.9-14.4    c-8.85,0-16.5,5.851-18.9,14.4c-6.3,22.95-8.1,24.6-31.05,31.05c-8.4,2.4-14.4,10.05-14.4,18.9c0,8.85,5.85,16.5,14.4,18.899    c22.95,6.301,24.6,8.101,31.05,31.051c2.4,8.399,10.05,14.399,18.9,14.399c8.85,0,16.5-5.85,18.9-14.399    c6.3-22.95,8.1-24.601,31.05-31.051c8.4-2.399,14.4-10.05,14.4-18.899C200.4,635.6,194.55,627.8,186.15,625.55z M136.2,675.8    c-6.3-17.55-13.95-25.2-31.5-31.5c17.55-6.3,25.2-13.95,31.5-31.5c6.3,17.55,13.95,25.2,31.5,31.5    C150.15,650.6,142.5,658.4,136.2,675.8z"></path><path d="M598.35,369.65c-4.199-4.95-11.55-5.55-16.5-1.5c-10.199,8.55-21,16.5-32.1,23.55c-8.85-10.65-22.35-17.4-37.35-17.4    c-26.7,0-48.45,21.45-48.45,47.851c0,2.399,0.149,4.8,0.6,7.05c-11.85,3.149-23.85,5.7-36.149,7.5v-10.05    c0-10.801-8.851-19.65-19.65-19.65H359.4c-10.801,0-19.65,8.85-19.65,19.65v10.05c-12.3-1.8-24.3-4.2-36.15-7.5    c0.3-2.4,0.6-4.8,0.6-7.2c0-26.4-21.75-47.85-48.45-47.85c-15,0-28.35,6.75-37.35,17.4c-83.7-53.55-139.2-146.4-139.2-252    c0-6.3-5.25-11.55-11.7-11.55s-11.7,5.25-11.7,11.7c0,114.75,60.9,215.85,152.55,273.3c-0.6,3-0.9,6-0.9,9.15    c0,26.399,21.75,47.85,48.45,47.85c15.75,0,29.7-7.5,38.55-18.9c14.7,4.2,30,7.351,45.45,9.45v20.4    c-71.851,19.5-124.8,85.2-124.8,163.2c0,93.149,75.9,169.05,169.05,169.05c32.399,0,63.75-9.15,90.899-26.55    c5.4-3.45,7.05-10.801,3.601-16.2c-3.45-5.4-10.801-7.05-16.2-3.601c-4.05,2.551-8.25,4.95-12.601,7.2l-13.5-33L480.15,697.4    l33.3,13.949c-5.851,11.25-13.05,21.75-21.75,31.2c-4.351,4.8-4.05,12.15,0.75,16.65c4.8,4.35,12.149,4.05,16.649-0.75    c13.051-14.4,23.551-30.601,30.9-48.15c0-0.149,0.15-0.3,0.15-0.45c8.55-20.55,13.199-42.75,13.199-65.399    c0-77.851-52.949-143.7-124.8-163.2v-20.4c15.45-2.1,30.601-5.25,45.45-9.449c8.85,11.55,22.8,18.899,38.55,18.899    c26.7,0,48.45-21.45,48.45-47.85c0-3.15-0.3-6.15-0.9-9c12.9-8.101,25.351-17.101,37.051-26.85    C601.8,382.1,602.55,374.6,598.35,369.65z M255.75,446.6c-13.8,0-24.9-10.949-24.9-24.3c0-13.35,11.1-24.3,24.9-24.3    s24.9,10.95,24.9,24.3C280.65,435.65,269.4,446.6,255.75,446.6z M363.3,430.7h41.55v45.6c-6.75-0.899-13.8-1.35-20.699-1.35    c-7.051,0-13.95,0.45-20.7,1.35L363.3,430.7L363.3,430.7z M318.3,514.25l15,36.6l-41.7,41.7l-36.75-15.45    C268.95,550.1,291.15,528.05,318.3,514.25z M238.5,644c0-15.9,2.55-31.05,7.2-45.3l34.65,14.55v60.3l-35.1,14.4    C240.9,674.15,238.5,659.45,238.5,644z M316.95,773.15c-27-14.101-49.05-36.301-62.85-63.45l33-13.5l43.8,43.8L316.95,773.15z     M384,789.65c-15.9,0-31.05-2.551-45.3-7.2l13.5-32.25H414.6l13.351,32.7C413.85,787.25,399,789.65,384,789.65z M529.5,644    c0,15.6-2.55,30.9-7.2,45.3l-34.649-14.55v-7.65c0-6.449-5.25-11.699-11.7-11.699s-11.7,5.25-11.7,11.699V679.7l-46.95,46.95H351    l-46.95-46.95v-66.3L351,566.45h66.3l46.95,46.95v8.25c0,6.449,5.25,11.699,11.7,11.699s11.7-5.25,11.7-11.699v-7.2l35.25-14.4    C527.1,614,529.5,628.7,529.5,644z M451.05,515c27,14.1,49.05,36.3,62.851,63.45l-36.601,15l-41.7-41.7L451.05,515z M429.3,505.85    l-15.6,37.2h-58.2L340.05,505.4c13.8-4.351,28.65-6.75,43.95-6.75C399.9,498.5,415.05,501.05,429.3,505.85z M512.25,446.6    c-13.8,0-24.9-10.949-24.9-24.3c0-13.35,11.101-24.3,24.9-24.3s24.9,10.95,24.9,24.3C537.15,435.65,526.05,446.6,512.25,446.6z"></path></svg></div>
                                    <div class="list-why-choose-us__title"><?php the_sub_field('title_2'); ?></div>
                                </div><a class="list-why-choose-us__back" href="#"><span class="list-why-choose-us__title"><?php the_sub_field('title_2'); ?></span>
                                    <span class="list-why-choose-us__text"><?php the_sub_field('text_2'); ?></span></a></div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-12">
                            <div class="list-why-choose-us__item">
                                <div class="list-why-choose-us__front">
                                    <div class="list-why-choose-us__svg"><svg viewbox="0 128.9 768 766.2"><path fill="#DFBC82" d="M154.2,193.25c-33.9-9.45-37.35-13.05-46.8-47.55c-2.7-9.9-11.85-16.8-22.05-16.8    c-10.2,0-19.2,6.9-21.9,16.8c-9.45,34.5-12.9,38.1-46.8,47.55C6.9,196.1,0,205.1,0,215.3c0,10.2,6.9,19.35,16.65,22.2    c33.9,9.45,37.35,13.05,46.8,47.55c2.7,9.9,11.7,16.8,21.9,16.8c10.2,0,19.2-6.9,21.9-16.8c9.45-34.5,12.9-38.1,46.8-47.55    c9.75-2.7,16.65-11.85,16.65-22.05C170.7,205.25,163.95,196.1,154.2,193.25z M85.35,276.05c-10.95-39.3-21.3-49.5-60.45-60.75    c39-11.25,49.35-21.45,60.45-60.75c10.95,39.3,21.3,49.5,60.45,60.75C106.65,226.55,96.45,236.9,85.35,276.05z"></path><path fill="#DFBC82" d="M752.7,801.2c-27.3-7.65-29.851-10.2-37.351-37.95c-2.55-9.15-10.8-15.45-20.1-15.45s-17.7,6.3-20.1,15.45    c-7.5,27.9-10.051,30.3-37.351,37.95c-9,2.55-15.3,10.8-15.3,20.25s6.3,17.7,15.3,20.25c27.3,7.649,29.851,10.2,37.351,37.95    c2.399,9.149,10.8,15.449,20.1,15.449s17.7-6.3,20.1-15.449C723,851.75,725.4,849.35,752.7,841.7c9-2.55,15.3-10.8,15.3-20.25    S761.7,803.75,752.7,801.2z M695.25,864.65c-8.1-25.95-17.1-34.95-43.05-43.2c25.95-8.25,34.95-17.25,43.05-43.2    c8.1,25.95,17.1,34.95,43.05,43.2C712.35,829.7,703.35,838.7,695.25,864.65z"></path><path d="M712.95,597.8L661.2,518.15c-5.101-7.95-13.8-12.601-23.101-12.601h-13.05L639.3,488c10.5-12.9,12-30.6,4.2-45    c-0.45-0.9-93.6-144.15-93.6-144.15c-7.351-11.4-19.95-18.15-33.45-18.15H351h-0.3H185.25c-13.5,0-25.95,6.75-33.45,18.15    L60,440.3c-9.6,14.851-8.7,34.05,2.55,47.851l95.7,117.449c4.05,5.101,11.55,5.851,16.5,1.65c5.1-4.05,5.85-11.55,1.65-16.5    l-93.3-114.6h148.05l97.95,302.1l-124.5-153c-4.05-5.1-11.55-5.85-16.5-1.65c-5.1,4.051-5.85,11.551-1.65,16.5l133.8,164.25    c7.65,9.301,18.9,14.7,30.9,14.7s23.25-5.399,30.899-14.7l69-84.75l72.75,89.25c5.25,6.45,13.05,10.2,21.3,10.2    s16.051-3.75,21.301-10.2L711.45,630.65C718.95,621.35,719.7,608,712.95,597.8z M464.85,529.1h58.2l-29.1,44.851L464.85,529.1z     M480,595.4l-4.5,6.899h-73.65l41.4-63.6L480,595.4z M461.1,505.55l9.601-29.399h148.05l-24,29.399h-49.8h-0.15H461.1z     M544.8,538.7l41.4,63.6h-82.8L544.8,538.7z M566.55,529.1h58.2l-29.1,44.851L566.55,529.1z M531.45,313.85l90.3,138.9H469.2    l-13.95-21.45L531.45,313.85z M509.7,304.25L441.15,409.7L372.6,304.25H509.7z M329.1,304.25L260.55,409.7L192,304.25H329.1z     M232.65,452.6H79.95l90.3-138.899l76.35,117.45L232.65,452.6z M255.9,476.15h82.65c6.45,0,11.7-5.25,11.7-11.7    s-5.25-11.7-11.7-11.7H260.7l90.3-138.9l90.3,138.9h-55.5c-6.45,0-11.7,5.25-11.7,11.7s5.25,11.7,11.7,11.7h60.3l-11.55,35.399    c-2.25,1.8-4.35,4.05-6,6.601L376.8,597.8c-6.6,10.2-6,23.55,1.8,33l12.45,15.3L351.15,769.4L255.9,476.15z M372.75,778.25    L408.6,667.7l27,33.3L372.75,778.25z M404.85,626H473.7l45.6,140.55L404.85,626z M570.45,766.4l18.3-56.4    c1.95-6.15-1.35-12.75-7.5-14.85c-6.15-1.95-12.75,1.35-14.85,7.5l-21.601,66.6l-46.5-143.4h93L581.1,657.2    c-1.949,6.149,1.351,12.75,7.5,14.85c1.2,0.45,2.4,0.601,3.601,0.601c4.95,0,9.6-3.15,11.25-8.101L615.9,626h68.85L570.45,766.4z     M614.25,602.45l-4.5-6.9l36.9-56.7l41.399,63.601H614.25z"></path></svg></div>
                                    <div class="list-why-choose-us__title"><?php the_sub_field('title_3'); ?></div>
                                </div><a class="list-why-choose-us__back" href="#"><span class="list-why-choose-us__title"><?php the_sub_field('title_3'); ?></span>
                                    <span class="list-why-choose-us__text"><?php the_sub_field('text_3'); ?></span></a></div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'design_your_perfect__diamond_ring' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="create-block">
                <div class="container">
                    <div class="row" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-once="true">
                        <div class="col-xl-6">
                            <div class="create-block__title"><?php the_sub_field('title'); ?></div>
                            <div class="create-block__title-text"><?php the_sub_field('description'); ?></div>
                            <div class="create-block__block-button">
                                <a class="button button_primary" href="<?php the_sub_field('button_1_url'); ?>"><?php the_sub_field('button_1_text'); ?></a>
                                <a class="button button_light" href="<?php the_sub_field('button_2_url'); ?>">Fi<?php the_sub_field('button_2_text'); ?></a></div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'award_winning_designer' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="about-me">
                <div class="container">
                    <div class="row" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-once="true">
                        <div class="first-col col-xl-4 col-lg-4">
                            <div class="first-col__title"><?php the_sub_field('title'); ?></div>
                            <div class="first-col__item"><img class="first-col__item__photo" src="<?php the_sub_field('image'); ?>" alt="" role="presentation" /></div>
                            <div class="photo-title">
                                <div class="photo-title__name"><?php the_sub_field('about_me_name'); ?></div>
                                <div class="photo-title__sub-name"><?php the_sub_field('about_me_sub_name'); ?></div>
                            </div>
                        </div>
                        <div class="second-col col-xl-8 col-lg-8">
                            <div class="second-col__title"><?php the_sub_field('title'); ?></div>
                          <?php the_sub_field('description'); ?>
                            <a class="button button_primary" href="<?php the_sub_field('button_url'); ?>"><?php the_sub_field('button_text'); ?></a>
                            <img class="transparent-icon" src="<?php echo get_template_directory_uri()?>/assets/img/about-me-transparent-icon.png" alt="" role="presentation"
                            /></div>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'services_home' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="services">
                <div class="container" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-once="true">
                    <div class="services__title"><?php the_sub_field('title'); ?></div>
                    <div class="services__sub-title"><?php the_sub_field('description'); ?></div>
                    <div class="list-services tabs-js">
                        <div class="list-services__tabs js-tabs-items">
                            <div class="list-services__tabs-items js-tabs-item active"><?php the_sub_field('title_1'); ?></div>
                            <div class="list-services__tabs-items js-tabs-item"><?php the_sub_field('title_2'); ?></div>
                            <div class="list-services__tabs-items js-tabs-item"><?php the_sub_field('title_3'); ?></div>
                            <div class="list-services__tabs-items js-tabs-item"><?php the_sub_field('title_4'); ?></div>
                        </div>
                        <div class="list-services__content js-content">
                            <div class="list-services__tabs-content js-tabs-content active">
                                <div class="col-xl-5 col-lg-5 col-md-12">
                                    <div class="slider-tabs-rooms" style="background-image: url(<?php the_sub_field('image_1'); ?>);"></div>
                                </div>
                                <div class="col-xl-7 col-lg-7 col-md-12">
                                    <div class="list-services__block-info">
                                        <div class="list-services__title-tabs"><?php the_sub_field('title_1'); ?></div>
                                        <div class="list-services__text">
                                          <?php the_sub_field('text_1'); ?>
                                        </div><a class="button button_primary" href="<?php the_sub_field('url_1'); ?>">READ MORE</a></div>
                                </div>
                            </div>
                            <div class="list-services__tabs-content js-tabs-content">
                                <div class="col-xl-5 col-lg-5 col-md-12">
                                    <div class="slider-tabs-rooms" style="background-image: url(<?php the_sub_field('image_2'); ?>);"></div>
                                </div>
                                <div class="col-xl-7 col-lg-7 col-md-12">
                                    <div class="list-services__block-info">
                                        <div class="list-services__title-tabs"><?php the_sub_field('title_2'); ?></div>
                                        <div class="list-services__text">
                                          <?php the_sub_field('text_2'); ?>
                                        </div><a class="button button_primary" href="<?php the_sub_field('url_2'); ?>">READ MORE</a></div>
                                </div>
                            </div>
                            <div class="list-services__tabs-content js-tabs-content">
                                <div class="col-xl-5 col-lg-5 col-md-12">
                                    <div class="slider-tabs-rooms" style="background-image: url(<?php the_sub_field('image_3'); ?>);"></div>
                                </div>
                                <div class="col-xl-7 col-lg-7 col-md-12">
                                    <div class="list-services__block-info">
                                        <div class="list-services__title-tabs"><?php the_sub_field('title_3'); ?></div>
                                        <div class="list-services__text">
                                          <?php the_sub_field('text_3'); ?>
                                        </div><a class="button button_primary" href="<?php the_sub_field('url_3'); ?>">READ MORE</a></div>
                                </div>
                            </div>
                            <div class="list-services__tabs-content js-tabs-content">
                                <div class="col-xl-5 col-lg-5 col-md-12">
                                    <div class="slider-tabs-rooms" style="background-image: url(<?php the_sub_field('image_4'); ?>);"></div>
                                </div>
                                <div class="col-xl-7 col-lg-7 col-md-12">
                                    <div class="list-services__block-info">
                                        <div class="list-services__title-tabs"><?php the_sub_field('title_4'); ?></div>
                                        <div class="list-services__text">
                                          <?php the_sub_field('text_4'); ?>
                                        </div><a class="button button_primary" href="<?php the_sub_field('url_4'); ?>">READ MORE</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="nav-arrows__left prev-tabs"><svg viewbox="90.838 84.5 430.324 617.5"><path d="M97.31,289.887c-8.629,8.337-8.629,22.214,0,30.843c8.338,8.337,22.214,8.337,30.532,0l156.391-156.392  v516.07c0.02,12.03,9.582,21.592,21.612,21.592s21.902-9.562,21.902-21.592v-516.07l156.101,156.392  c8.629,8.337,22.524,8.337,30.843,0c8.629-8.629,8.629-22.525,0-30.843L321.275,96.472c-8.337-8.629-22.213-8.629-30.532,0  L97.31,289.887z"></path></svg></div>
                        <div class="nav-arrows__right next-tabs"><svg viewbox="90.838 84.5 430.324 617.5"><path d="M97.31,289.887c-8.629,8.337-8.629,22.214,0,30.843c8.338,8.337,22.214,8.337,30.532,0l156.391-156.392  v516.07c0.02,12.03,9.582,21.592,21.612,21.592s21.902-9.562,21.902-21.592v-516.07l156.101,156.392  c8.629,8.337,22.524,8.337,30.843,0c8.629-8.629,8.629-22.525,0-30.843L321.275,96.472c-8.337-8.629-22.213-8.629-30.532,0  L97.31,289.887z"></path></svg></div>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'our_collections' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="shops" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-once="true" >
                <div class="container">
                    <div class="shops__title"><?php the_sub_field('title'); ?></div>
                    <div class="shops__sub-title"><?php the_sub_field('description'); ?></div>
                    <div class="list-shops">
                        <div class="col-xl-8 col-lg-12 col-md-12 col-sm-12">
                            <div class="list-shops__low-block row">
                                <div class="col-xl-6">
                                    <a class="list-shops__item list-shops__item_low" href="<?php the_sub_field('url_1'); ?>">
                                        <img class="list-shops__img" src="<?php the_sub_field('image_1'); ?>" alt="" role="presentation"/>
                                        <div class="list-shops__block"><div class="list-shops__title"><?php the_sub_field('title_1'); ?></div>
                                            <div class="list-shops__text"><?php the_sub_field('text_1'); ?></div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-xl-6">
                                    <a class="list-shops__item list-shops__item_low" href="<?php the_sub_field('url_2'); ?>">
                                        <img class="list-shops__img" src="<?php the_sub_field('image_2'); ?>" alt="" role="presentation"/>
                                        <div class="list-shops__block">
                                            <div class="list-shops__title"><?php the_sub_field('title_2'); ?></div>
                                            <div class="list-shops__text"><?php the_sub_field('text_2'); ?></div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-xl-6">
                                    <a class="list-shops__item list-shops__item_low" href="<?php the_sub_field('url_3'); ?>">
                                        <img class="list-shops__img" src="<?php the_sub_field('image_3'); ?>" alt="" role="presentation"/>
                                        <div class="list-shops__block">
                                            <div class="list-shops__title"><?php the_sub_field('title_3'); ?></div>
                                            <div class="list-shops__text"><?php the_sub_field('text_3'); ?></div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-xl-6">
                                    <a class="list-shops__item list-shops__item_low" href="<?php the_sub_field('url_4'); ?>">
                                        <img class="list-shops__img" src="<?php the_sub_field('image_4'); ?>" alt="" role="presentation"/>
                                        <div class="list-shops__block">
                                            <div class="list-shops__title"><?php the_sub_field('title_4'); ?></div>
                                            <div class="list-shops__text"><?php the_sub_field('text_4'); ?></div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12">
                            <a class="list-shops__item" href="<?php the_sub_field('url_5'); ?>">
                                <img class="list-shops__img" src="<?php the_sub_field('image_5'); ?>" alt="" role="presentation"/>
                                <div class="list-shops__block">
                                    <div class="list-shops__title"><?php the_sub_field('title_5'); ?></div>
                                    <div class="list-shops__text"><?php the_sub_field('text_5'); ?></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xl-6">
                            <a class="list-shops__item list-shops__item_long" href="<?php the_sub_field('url_6'); ?>">
                                <img class="list-shops__img" src="<?php the_sub_field('image_6'); ?>" alt="" role="presentation"/>
                                <div class="list-shops__block">
                                    <div class="list-shops__title"><?php the_sub_field('title_6'); ?></div>
                                    <div class="list-shops__text"><?php the_sub_field('text_6'); ?></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xl-6">
                            <a class="list-shops__item list-shops__item_long" href="<?php the_sub_field('url_7'); ?>">
                                <img class="list-shops__img" src="<?php the_sub_field('image_7'); ?>" alt="" role="presentation"/>
                                <div class="list-shops__block">
                                    <div class="list-shops__title"><?php the_sub_field('title_7'); ?></div>
                                    <div class="list-shops__text"><?php the_sub_field('text_7'); ?></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'slider' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="main">
                <div class="slider-main">

				  <?php
				  if( have_rows('slider_repeater') ):
					while ( have_rows('slider_repeater') ) : the_row(); ?>

                      <?php $type = get_sub_field('select_type'); ?>

                     <?php if ( $type === 'video' ){ ?>

                            <div class="slider-main__item">
                                <div class="slider-main__video-container">
                                    <iframe id="playerVideo" width="100%" height="1080" src="https://www.youtube.com/embed/<?php the_sub_field('type');?>?rel=0&amp;controls=0&amp;showinfo=0&amp;autoplay=1&amp;loop=1&amp;mute=1&amp;playlist=<?php the_sub_field('type');?>&amp;enablejsapi=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
                                </div>
                                <div class="container">
                                    <div class="slider-main__title"><?php the_sub_field('title'); ?></div>
                                    <div class="slider-main__sub-title"><?php the_sub_field('description'); ?></div>
                                    <div class="slider-main__block-button">
                                        <a class="button button_primary" href="<?php the_sub_field('button_url'); ?>"><?php the_sub_field('button_text'); ?></a>
                                    </div>
                                </div>
                            </div>

                          <?php } ?>

					<?php if ( $type === 'image' ){ ?>

                            <div class="slider-main__item" style="background-image: url(<?php the_sub_field('image'); ?>);">
                                <div class="container">
                                    <div class="slider-main__title"><?php the_sub_field('title'); ?></div>
                                    <div class="slider-main__sub-title"><?php the_sub_field('description'); ?></div>
                                    <div class="slider-main__block-button">
                                        <a class="button button_primary" href="<?php the_sub_field('button_url'); ?>"><?php the_sub_field('button_text'); ?></a>
                                    </div>
                                </div>
                            </div>
                          <?php } ?>

					<?php  endwhile;
				  else :
				  endif;
				  ?>

                </div>
                <div class="container wrap-arrows">
                    <div class="nav-arrows-slider-main">
                        <div class="nav-arrows-slider-main__left"><svg viewbox="90.838 84.5 430.324 617.5"><path d="M97.31,289.887c-8.629,8.337-8.629,22.214,0,30.843c8.338,8.337,22.214,8.337,30.532,0l156.391-156.392  v516.07c0.02,12.03,9.582,21.592,21.612,21.592s21.902-9.562,21.902-21.592v-516.07l156.101,156.392  c8.629,8.337,22.524,8.337,30.843,0c8.629-8.629,8.629-22.525,0-30.843L321.275,96.472c-8.337-8.629-22.213-8.629-30.532,0  L97.31,289.887z"></path></svg></div>
                        <div class="nav-arrows-slider-main__active"><span>1</span>/<span>4</span></div>
                        <div class="nav-arrows-slider-main__right"><svg viewbox="90.838 84.5 430.324 617.5"><path d="M97.31,289.887c-8.629,8.337-8.629,22.214,0,30.843c8.338,8.337,22.214,8.337,30.532,0l156.391-156.392  v516.07c0.02,12.03,9.582,21.592,21.612,21.592s21.902-9.562,21.902-21.592v-516.07l156.101,156.392  c8.629,8.337,22.524,8.337,30.843,0c8.629-8.629,8.629-22.525,0-30.843L321.275,96.472c-8.337-8.629-22.213-8.629-30.532,0  L97.31,289.887z">           </path></svg></div>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'generations_of_commitment_to_you' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <div class="first-block">
                <div class="container">
                    <div class="wrapper">
                        <div class="first-block__title"><?php the_sub_field('title'); ?></div>
                        <div class="first-block__sub-title"><?php the_sub_field('sub_title'); ?></div>
                    </div>
                    <div class="people">
                        <div class="row">

                          <?php
                          if( have_rows('generations_of_commitment_to_you_repetaer') ):
                            while ( have_rows('generations_of_commitment_to_you_repetaer') ) : the_row(); ?>

                                <div class="col-xl-3 col-lg-3 col-md-6">

                                    <div class="first-block__item">
                                        <div class="icon">
                                            <img src="<?php the_sub_field('image'); ?>">
                                        </div>
                                    </div>
                                    <div class="first-block__item first-block__item_title"><?php the_sub_field('name'); ?></div>
                                </div>


                            <?php  endwhile;
                          else :
                          endif;
                          ?>

                        </div>
                    </div>
                </div>
            </div>
        <?php endif;

        elseif( get_row_layout() == 'family_is_everything' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>

            <div class="second-block">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-5 col-lg-5">
                            <div class="second-block__wrapper-photo">
                                <div class="second-block__item photo-one">
                                    <img src="<?php the_sub_field('image_1'); ?>">
                                </div>
                                <div class="second-block__item photo-two">
                                    <img src="<?php the_sub_field('image_2'); ?>">
                                </div>
                                <div class="second-block__item photo-three">
                                    <img src="<?php the_sub_field('image_3'); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="story col-xl-7 col-lg-7">
                            <div class="second-block__title"><?php the_sub_field('title'); ?></div>
                            <div class="second-block__text">
                              <?php the_sub_field('text'); ?>
                            </div>
                            <ul class="second-block__list">
                              <?php the_sub_field('list'); ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>


        <?php endif;

        elseif( get_row_layout() == 'our_journey' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <div class="third-block">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-7">
                            <div class="third-block__title"><?php the_sub_field('title'); ?></div>
                            <div class="third-block__text">
                              <?php the_sub_field('text'); ?>
                            </div><a class="button" href="<?php the_sub_field('button_url'); ?>"><?php the_sub_field('button_text'); ?></a></div>
                        <div class="col-xl-5">
                            <div class="third-block__wrapper-photo">
                                <div class="third-block__item photo-one">
                                    <img src="<?php the_sub_field('image_1'); ?>">
                                </div>
                                <div class="third-block__item photo-two">
                                    <img src="<?php the_sub_field('image_2'); ?>">
                                </div>
                                <div class="third-block__item photo-three">
                                    <img src="<?php the_sub_field('image_3'); ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif;

        elseif( get_row_layout() == 'meet_the_team' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>

            <div class="fifth-block">
                <div class="container">
                    <div class="fifth-block__title"><?php the_sub_field('title'); ?></div>
                    <div class="fifth-block__sub-title"><?php the_sub_field('description'); ?></div>
                    <div class="row">
                        <div class="col-xl-10 offset-xl-1 col-lg-10 offset-lg-1">
                            <div class="fifth-block__item">
                                <img src="<?php the_sub_field('image'); ?>">
                            </div>
                        </div>
                    </div><a class="button" href="<?php the_sub_field('button_url'); ?>"><?php the_sub_field('button_text'); ?></a></div>
            </div>

        <?php endif;

        elseif( get_row_layout() == 'watch_the_video' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>

            <section class="video-block" style="background-image: url(<?php the_sub_field('background_image'); ?>);">
                <div class="container">
                    <div class="video-block__wrapper">
                            <a class="button button_primary click-modal-video" href="#"><i class="fa fa-play" aria-hidden="true"></i><?php the_sub_field('button_text'); ?></a>
                    </div>
                </div>
                <div class="modal-video">
                    <div class="modal-video__wrapper">
                        <div class="modal-video__close"></div>
                        <div class="modal-video__iframe">
                            <iframe width="100%" height="620" src="https://www.youtube.com/embed/<?php the_sub_field('video_yotobe_id'); ?>?rel=0&amp;showinfo=0?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
                        </div>
                    </div>
                    <div class="bg-overlay-video"></div>
                </div>
            </section>

        <?php endif;

        elseif( get_row_layout() == 'our_heroes' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>

            <div class="eighth-block">
                <div class="container">
                    <div class="eighth-block__title"><?php the_sub_field('title'); ?></div>
                    <div class="eighth-block__sub-title"><?php the_sub_field('description'); ?></div>
                    <div class="eighth-block__wish"><?php the_sub_field('text'); ?></div>
                    <div class="people">
                        <div class="row">

                          <?php
                          if( have_rows('our_heroes_heroes') ):
                            while ( have_rows('our_heroes_heroes') ) : the_row(); ?>

                                <div class="col-xl-3 col-lg-3 col-md-6">
                                    <div class="eighth-block__item">
                                        <div class="icon">
                                            <img src="<?php the_sub_field('image'); ?>">
                                        </div>
                                        <div class="eighth-block__item eighth-block__item_title"><?php the_sub_field('text'); ?></div>
                                    </div>
                                </div>

                            <?php  endwhile;
                          else :
                          endif;
                          ?>

                        </div>
                    </div><a class="button" href="<?php the_sub_field('button_url'); ?>"><?php the_sub_field('button_text'); ?></a></div>
            </div>

        <?php endif;

                endif;
            endwhile;
          endif;

        ?>